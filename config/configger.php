<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Settings storage driver
    |--------------------------------------------------------------------------
    |
    | Here you can set the driver for storing your settings.
    | Supported drivers:
    |   "database" - Store settings in database
    |   "redis" - Store settings in Redis storage
    |   "json" - Store settings in JSON-format in file
    |   "memory" - Store settings in memory (Only for one request)
    |
    */
    'driver' => 'database',

    /*
    |--------------------------------------------------------------------------
    | Specific storage drivers settings
    |--------------------------------------------------------------------------
    |
    | To make settings specific to each driver separately.
    |
    */
    'drivers' => [
        'database' => [
            // Name of table with settings
            'table' => 'configger'
        ],

        'redis' => [
            // Name of Redis connection
            'connection' => 'default',
            // Options prefix
            'prefix' => 'configger'
        ],

        'json' => [
            // Full path to JSON-file
            'file' => storage_path('app/configger.json')
        ]
    ]
];

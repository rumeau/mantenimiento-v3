@extends('layouts.app')

@section('breadcrumb')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Inicio</li>
        <li class="breadcrumb-item active">Perfil</li>
    </ol>
@endsection

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <strong>Perfil</strong>
                </div>
                <div class="card-block">

                    {!! Form::model($user, ['route' => ['profile.save']]) !!}

                    <div class="row">
                        <div class="col-sm-12 {{ $errors->has('name') ? 'has-danger' : '' }}">
                            <div class="form-group">
                                <label for="name" class="col-form-label">Nombre</label>
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) !!}
                                @if ($errors->has('name'))
                                    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--/.row-->

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="name" class="col-form-label">E-mail</label>
                                <p class="form-control-static">{{ $user->email  }}</p>
                            </div>
                        </div>
                    </div>
                    <!--/.row-->

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="mobile" class="col-form-label">Celular</label>
                                <div class="input-group">
                                    <span class="input-group-addon">+56 9</span>
                                    {!! Form::text('mobile', null, ['class' => 'form-control', 'placeholder' => '99999999', 'maxlength' => 8]) !!}
</div>
                            </div>
                        </div>
                    </div>
                    <!--/.row-->

                    <fieldset>
                        <legend>Clave</legend>

                        <div class="row">
                            <div class="col-sm-12 {{ $errors->has('password_old') ? 'has-danger' : '' }}">
                                <div class="form-group">
                                    <label for="name">Clave Actual</label>
                                    {!! Form::password('password_old', ['class' => 'form-control']) !!}
                                    @if ($errors->has('password_old'))
                                        {!! $errors->first('password_old', '<span class="help-block">:message</span>') !!}
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!--/.row-->

                        <div class="row">
                            <div class="col-sm-12 {{ $errors->has('password') ? 'has-danger' : '' }}">
                                <div class="form-group">
                                    <label for="name">Clave Nueva</label>
                                    {!! Form::password('password', ['class' => 'form-control']) !!}
                                    @if ($errors->has('password'))
                                        {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!--/.row-->

                        <div class="row">
                            <div class="col-sm-12 {{ $errors->has('password_confirmation') ? 'has-danger' : '' }}">
                                <div class="form-group">
                                    <label for="name">Confirmar Clave Nueva</label>
                                    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                                    @if ($errors->has('password_confirmation'))
                                        {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!--/.row-->
                    </fieldset>

                    <div class="row">
                        <div class="col-sm-12 text-right">
                            {!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                    <!--/.row-->
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
        <!--/.col-->
    </div>
    <!--/.row-->

@endsection

@push('inline-scripts')
<script type="text/javascript">
    $(function() {
        let officeDiv = $('#officeDiv');

        $('#role').on('change', function() {
            let el = $(this);
            if (_.includes(['client', 'company'], el.val())) {
                console.log('muestra');
                officeDiv.show();
            } else {
                console.log('oculta');
                officeDiv.hide();
            }
        });
    });
</script>
@endpush

@extends('layouts.app')

@section('breadcrumb')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Inicio</li>
        <li class="breadcrumb-item active">Sedes</li>
    </ol>
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-5">
                            <h4 class="card-title"><small><i class="fa fa-align-justify"></i></small> Sedes</h4>
                        </div>
                        <div class="col-sm-7">
                            <div class="btn-toolbar pull-right" role="toolbar" aria-label="Toolbar with button groups">
                                <div class="btn-group hidden-sm-down" role="group" aria-label="Second group">
                                    <a class="btn btn-primary" href="{{ route('offices.create') }}"><i class="icon-plus"></i> &nbsp;Nueva Sede</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="offset-md-6 col-md-6 offset-lg-8 col-lg-4 text-right">
                            {!! Form::select('client', clients_array_select(), null, ['class' => 'form-control', 'placeholder' => '-- Cliente --', 'id' => 'clientFilter']) !!}
                        </div>
                    </div>

                    <div id="jsGrid"></div>
                </div>
            </div>
        </div>
    </div>
    <!--/.row-->

@endsection

@section('scripts')
    @parent

    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.css" />
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid-theme.min.css" />

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.js"></script>
@endsection

@push('inline-scripts')
<script type="text/javascript">
    var grid = $("#jsGrid").jsGrid({
        height: "auto",
        width: "100%",

        filtering: false,
        sorting: true,
        autoload: true,
        paging: true,
        pageLoading: true,
        pageSize: 20,
        pageIndex: 1,

        pagerFormat: "Páginas: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} de {pageCount}",
        pagePrevText: "Anterior",
        pageNextText: "Siguiente",
        pageFirstText: "Primera",
        pageLastText: "Ultima",
        pageNavigatorNextText: "...",
        pageNavigatorPrevText: "...",
        pagerRenderer: null,

        controller: {
            loadData: function(filter) {
                return $.ajax({
                    url: laroute.route('offices'),
                    dataType: "json",
                    data: {
                        page: filter.pageIndex,
                        limit: filter.pageSize,
                        filter: $.extend(filter, {client_id: $('#clientFilter').val()})
                    }
                });
            }
        },

        fields: [
            { name: "id", title: "#", css: "hidden-sm hidden-smd", type: "text", width: 4, filtering: false },
            { name: "name", title: "Nombre", type: "text", filtering: false },
            { name: "client_id", title: "Cliente", css: "hidden-sm hidden-smd", width: 30, type: "select", filtering: false },
            { width: 90, align: "center", sorting: false, filtering: false,
                itemTemplate: function(value, item) {
                    return '<a href="' + laroute.route('offices.edit', {office: item.id}) + '">Editar</a>@group('admins') | <a href="' + laroute.route('offices.delete', {office: item.id}) + '">Eliminar</a>@endgroup';
                }
            }
        ]
    });

    $('#clientFilter').change(function() {
        $("#jsGrid").jsGrid("loadData", { client_id: $(this).val() });
    });
</script>
@endpush
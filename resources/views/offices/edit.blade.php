@extends('layouts.app')

@section('breadcrumb')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Inicio</li>
        <li class="breadcrumb-item">Sedes</li>
        <li class="breadcrumb-item">Editar</li>
        <li class="breadcrumb-item active">{{ $office->name }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <strong>Sedes</strong>
                    <small>Editar</small>
                </div>
                <div class="card-block">

                    {!! Form::model($office, ['route' => ['offices.update', $office]]) !!}

                    <div class="row">
                        <div class="col-sm-12 {{ $errors->has('name') ? 'has-danger' : '' }}">
                            <div class="form-group">
                                <label for="name">Nombre</label>
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
                                @if ($errors->has('name'))
                                    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--/.row-->

                    <div class="row">
                        <div class="col-sm-12 {{ $errors->has('phone') ? 'has-danger' : '' }}">
                            <div class="form-group">
                                <label for="name">Teléfono</label>
                                {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Teléfono']) !!}
                                @if ($errors->has('phone'))
                                    {!! $errors->first('phone', '<span class="help-block">:message</span>') !!}
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--/.row-->

                    <div class="row">
                        <div class="col-sm-12 {{ $errors->has('address') ? 'has-danger' : '' }}">
                            <div class="form-group">
                                <label for="name">Dirección</label>
                                {!! Form::textarea('address', null, ['class' => 'form-control', 'placeholder' => 'Dirección']) !!}
                                @if ($errors->has('address'))
                                    {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--/.row-->

                    <div class="row">
                        <div class="col-sm-12 {{ $errors->has('client_id') ? 'has-danger' : '' }}">
                            <div class="form-group">
                                <label for="name">Cliente</label>
                                {!! Form::select('client_id', $clients->pluck('name', 'id'), null, ['class' => 'form-control', 'id' => 'client', 'placeholder' => '-- Cliente --']) !!}
                                @if ($errors->has('client_id'))
                                    {!! $errors->first('client_id', '<span class="help-block">:message</span>') !!}
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--/.row-->

                    <div class="row">
                        <div class="col-sm-12 {{ $errors->has('users') ? 'has-danger' : '' }}">
                            <div class="form-group">
                                <label for="name">Usuarios</label>
                                {!! Form::select('users[]', [], null, ['class' => 'form-control',  'multiple' => true, 'id' => 'users', 'size' => 15]) !!}
                                @if ($errors->has('users'))
                                    {!! $errors->first('users', '<span class="help-block">:message</span>') !!}
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--/.row-->

                    <div class="row">
                        <div class="col-sm-6">
                            <a href="{{ route('offices') }}">&larr; Volver</a>
                        </div>
                        <div class="col-sm-6 text-right">
                            {!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                    <!--/.row-->
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
        <!--/.col-->
    </div>
    <!--/.row-->

@endsection

@push('inline-scripts')
    <script type="text/javascript">
        $(function() {
            let usersSelect = $('#users'),
                clientSelect = $('#client'),
                usersValues = {!! $office->users()->pluck('id')->toJson() !!};

            clientSelect.on('change', function () {
                usersSelect.empty();
                usersSelect.val('');

                $.ajax({
                    url: laroute.route('offices.client.users', {client: clientSelect.val()}),
                    dataType: 'json'
                }).then(function (response) {
                    $.each(response, function (i, item) {
                        let option = $('<option>').attr('value', item.id).append(item.name);
                        if (_.includes(usersValues, item.id)) {
                            option.attr('selected', true);
                        }

                        usersSelect.append(option);
                    });
                });
            });

            clientSelect.trigger('change');
        });
    </script>
@endpush
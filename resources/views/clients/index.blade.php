@extends('layouts.app')

@section('breadcrumb')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Inicio</li>
        <li class="breadcrumb-item active">Empresas</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-5">
                            <h4 class="card-title"><small><i class="fa fa-building"></i></small> Empresas</h4>
                        </div>
                        <div class="col-sm-7">
                            <div class="btn-toolbar pull-right" role="toolbar" aria-label="Toolbar with button groups">
                                <div class="btn-group hidden-sm-down" role="group" aria-label="Second group">
                                    <a class="btn btn-primary" href="{{ route('clients.create') }}"><i class="icon-plus"></i> &nbsp; Nueva Empresa</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="jsGrid"></div>
                    <div id="externalPager"></div>
                </div>
            </div>
        </div>
    </div>
    <!--/.row-->
@endsection

@section('scripts')
    @parent

    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.css" />
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid-theme.min.css" />

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.js"></script>
@endsection

@push('inline-scripts')
    <script type="text/javascript">
        $("#jsGrid").jsGrid({
            height: "auto",
            width: "100%",

            sorting: true,
            autoload: true,
            paging: true,
            pageLoading: true,
            pageSize: 15,
            pageIndex: 1,

            pagerFormat: "Páginas: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} de {pageCount}",
            pagePrevText: "Anterior",
            pageNextText: "Siguiente",
            pageFirstText: "Primera",
            pageLastText: "Ultima",
            pageNavigatorNextText: "...",
            pageNavigatorPrevText: "...",
            pagerRenderer: null,

            controller: {
                loadData: function(filter) {
                    return $.ajax({
                        url: laroute.route('clients'),
                        dataType: "json",
                        data: {
                            page: filter.pageIndex,
                            limit: filter.pageSize,
                            filter: filter
                        }
                    });
                }
            },

            fields: [
                { name: "id", title: "#", type: "text", width: 50 },
                { name: "name", title: "Nombre", type: "text", width: '100%', itemTemplate : function (value, item) {
                    if (item.trashed) {
                        return $('<del>').append(value);
                    }

                    return value;
                }},
                { width: 150, align: "center", sorting: false,
                    itemTemplate: function(value, item) {
                        if (item.trashed) {
                            return '<a href="' + laroute.route('clients.restore', {client: item.id}) + '">Restaurar</a>';
                        } else {
                            return '<a href="' + laroute.route('clients.edit', {client: item.id}) + '">Editar</a> | <a href="' + laroute.route('clients.delete', {client: item.id}) + '">Eliminar</a>';
                        }
                    }
                }
            ]
        });
    </script>
@endpush
@extends('layouts.app')

@section('breadcrumb')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Inicio</li>
        <li class="breadcrumb-item">Empresas</li>
        <li class="breadcrumb-item">Editar</li>
        <li class="breadcrumb-item active">{{ $client->name }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <strong>Empresa</strong>
                    <small>Editar</small>
                </div>
                <div class="card-block">

                    {!! Form::model($client, ['route' => ['clients.update', $client]]) !!}

                    <div class="row">
                        <div class="col-sm-12 {{ $errors->has('name') ? 'has-danger' : '' }}">
                            <div class="form-group">
                                <label for="name">Nombre</label>
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) !!}
                                @if ($errors->has('name'))
                                    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--/.row-->

                    <div class="row">
                        <div class="col-sm-12 {{ $errors->has('phone') ? 'has-danger' : '' }}">
                            <div class="form-group">
                                <label for="name">Teléfono</label>
                                {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Teléfono']) !!}
                                @if ($errors->has('phone'))
                                    {!! $errors->first('phone', '<span class="help-block">:message</span>') !!}
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--/.row-->

                    <div class="row">
                        <div class="col-sm-12 {{ $errors->has('address') ? 'has-danger' : '' }}">
                            <div class="form-group">
                                <label for="name">Dirección</label>
                                {!! Form::textarea('address', null, ['class' => 'form-control', 'placeholder' => 'Dirección']) !!}
                                @if ($errors->has('address'))
                                    {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 {{ $errors->has('users') ? 'has-danger' : '' }}">
                            <div class="form-group">
                                <label for="name">Usuarios</label>
                                {!! Form::select('users[]', $users->pluck('name', 'id'), null, ['class' => 'form-control', 'multiple' => true, 'size' => 15]) !!}
                                @if ($errors->has('users'))
                                    {!! $errors->first('users', '<span class="help-block">:message</span>') !!}
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <a href="{{ route('clients') }}">&larr; Volver</a>
                        </div>
                        <div class="col-sm-6 text-right">
                            {!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                    <!--/.row-->
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
        <!--/.col-->
    </div>
    <!--/.row-->

@endsection
@extends('layouts.app')

@section('breadcrumb')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Inicio</li>
        <li class="breadcrumb-item">Empresas</li>
        <li class="breadcrumb-item">Eliminar</li>
        <li class="breadcrumb-item active">{{ $client->name }}</li>
    </ol>
@endsection

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <strong>Empresa</strong>
                    <small>Eliminar</small>
                </div>
                <div class="card-block">

                    {!! Form::open(['route' => ['clients.destroy', $client]]) !!}

                        {!! Form::hidden('client_id', $client->id) !!}
                    <div class="row">
                        <div class="col-sm-12">
                            <p>Está seguro que desea eliminar la empresa con nombre <strong>"{{ $client->name }}"</strong></p>
                        </div>
                    </div>
                    <!--/.row-->

                    <div class="row">
                        <div class="col-sm-6">
                            <a href="{{ route('clients') }}">&larr; Volver</a>
                        </div>
                        <div class="col-sm-6 text-right">
                            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
                        </div>
                    </div>
                    <!--/.row-->
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
        <!--/.col-->
    </div>
    <!--/.row-->

@endsection
@extends('layouts.app')

@section('breadcrumb')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Inicio</li>
        <li class="breadcrumb-item">Orden de Trabajo</li>
        <li class="breadcrumb-item active">Crear</li>
    </ol>
@endsection

@section('content')
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Orden de Trabajo</strong>
                        <small>Crear</small>
                    </div>
                    <div class="card-block" id="otContainer">

                        {!! Form::open(['route' => 'jobs.store', 'files' => true]) !!}

                            <div class="row">
                                <div class="col-sm-12 {{ $errors->has('office') ? 'has-danger' : '' }}">
                                    <div class="form-group">
                                        <label for="office">Sede</label>
                                        {!! Form::select('office', $offices->pluck('name', 'id'), null, ['class' => 'form-control', 'placeholder' => '-- Sede --']) !!}
                                        @if ($errors->has('office'))
                                            {!! $errors->first('office', '<span class="help-block">:message</span>') !!}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!--/.row-->

                            <div class="row">
                                <div class="col-sm-12 {{ $errors->has('status') ? 'has-danger' : '' }}">
                                    <div class="form-group">
                                        <label for="name">Estado</label>
                                        {!! Form::select('status', [
                                            \App\Models\Job::STATUS_INPROGRESS => 'EN EJECUCION',
                                            \App\Models\Job::STATUS_PENDING => 'PENDIENTE DE COTIZACION',
                                        ], null, ['class' => 'form-control', 'placeholder' => '-- Estado --', 'id' => 'status']) !!}
                                        @if ($errors->has('status'))
                                            {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!--/.row-->

                            <div class="row">
                                <div class="col-sm-12 {{ $errors->has('job_type') ? 'has-danger' : '' }}">
                                    <div class="form-group">
                                        <label for="name">Tipo de OT</label>
                                        {!! Form::select('job_type', get_job_types(), null, ['class' => 'form-control']) !!}
                                        @if ($errors->has('job_type'))
                                            {!! $errors->first('job_type', '<span class="help-block">:message</span>') !!}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!--/.row-->

                            <div class="row">
                                <div class="col-sm-12 {{ $errors->has('title') ? 'has-danger' : '' }}">
                                    <div class="form-group">
                                        <label for="name">Título</label>
                                        {!! Form::text('title', null, ['class' => 'form-control']) !!}
                                        @if ($errors->has('title'))
                                            {!! $errors->first('title', '<span class="help-block">:message</span>') !!}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!--/.row-->

                            <fieldset>
                                <legend>Descripción</legend>

                                <div class="row">
                                    <div class="col-sm-12 {{ $errors->has('description') ? 'has-danger' : '' }}">
                                        <div class="form-group">
                                            <label for="name">Descripción</label>
                                            {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description', 'placeholder' => 'Descripción']) !!}
                                            @if ($errors->has('description'))
                                                {!! $errors->first('description', '<span class="help-block">:message</span>') !!}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <!--/.row-->

                                <div class="row">
                                    <div class="col-sm-12 {{ $errors->has('images') ? 'has-danger' : '' }}">
                                        <div class="form-group">
                                            <label for="name">Imagenes (Máximo 5)</label>
                                            <image-upload v-bind:items='{!! json_encode(old('images', [])) !!}'></image-upload>
                                            @if ($errors->has('images'))
                                                {!! $errors->first('images', '<span class="help-block">:message</span>') !!}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <!--/.row-->
                            </fieldset>

                            <fieldset>
                                <legend>Planificación</legend>

                                <div class="row">
                                    <div class="col-sm-6 {{ $errors->has('starting_at') ? 'has-danger' : '' }}">
                                        <div class="form-group">
                                            <label for="name">Fecha de Inicio</label>
                                            {!! Form::text('starting_at', null, ['class' => 'form-control', 'placeholder' => '', 'autocomplete' => 'off', 'data-toggle' => 'datepicker']) !!}
                                            @if ($errors->has('starting_at'))
                                                {!! $errors->first('starting_at', '<span class="help-block">:message</span>') !!}
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-6 {{ $errors->has('ending_at') ? 'has-danger' : '' }}">
                                        <div class="form-group">
                                            <label for="name">Fecha de Término</label>
                                            {!! Form::text('ending_at', null, ['class' => 'form-control', 'placeholder' => '', 'autocomplete' => 'off', 'data-toggle' => 'datepicker']) !!}
                                            @if ($errors->has('ending_at'))
                                                {!! $errors->first('ending_at', '<span class="help-block">:message</span>') !!}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <!--/.row-->
                            </fieldset>

                            <fieldset>
                                <legend>Detalle de Costos</legend>

                                <cost-details :items='{!! htmlspecialchars(json_encode(old('costs', [])), ENT_QUOTES, 'UTF-8') !!}'></cost-details>
                                @if ($errors->has('costs.*'))
                                    <span class="help-block">Deben completarse todos los campos de los costos agregados</span>
                                @endif
                            </fieldset>


                            <div class="row">
                                <div class="col-sm-6">
                                    <a href="{{ route('jobs') }}">&larr; Volver</a>
                                </div>
                                <div class="col-sm-6 text-right">
                                    {!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
                                </div>
                            </div>
                            <!--/.row-->
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
            <!--/.col-->
        </div>
        <!--/.row-->
@endsection

@section('scripts')
    @parent

    <!-- include summernote css/js-->
    <link href="{{ asset('js/vendor/wysiwyg/ui/trumbowyg.min.css') }}" rel="stylesheet">
    <script src="{{ asset('js/vendor/wysiwyg/trumbowyg.min.js') }}"></script>

    <!-- include datepicker css/js -->
    <link href="{{ asset('js/vendor/datepicker/dist/datepicker.min.css') }}" rel="stylesheet">
    <script src="{{ asset('js/vendor/datepicker/dist/datepicker.min.js') }}"></script>

@endsection

@push('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#description').trumbowyg();

            $('#status').on('change', function () {
                var el = $(this),
                    otContainer = $('#otContainer');
                if (el.val() == {{ \App\Models\Job::STATUS_PENDING }}) {
                    otContainer.addClass('otQuote');
                } else {
                    otContainer.removeClass('otQuote');
                }
            });

            $('[data-toggle="datepicker"]').datepicker({
                language: 'es',
                format: 'dd/mm/yyyy',
                autoHide: true
            });
        });
    </script>
@endpush

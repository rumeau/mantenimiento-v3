@extends('layouts.app')

@section('breadcrumb')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Inicio</li>
        <li class="breadcrumb-item">Orden de Trabajo</li>
        <li class="breadcrumb-item">Editar</li>
        <li class="breadcrumb-item active">#{{ $job->id }} - {{ $job->created_at->format('d/m/Y H:i:s') }}</li>
    </ol>
@endsection

@section('content')
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Orden de Trabajo</strong>
                        <small>Editar</small>
                    </div>
                    <div class="card-block otQuote" id="otContainer">

                        {!! Form::model($job, ['route' => ['jobs.update', $job->id], 'files' => true]) !!}

                            <div class="row">
                                <div class="col-sm-12 {{ $errors->has('office_id') ? 'has-danger' : '' }}">
                                    <div class="form-group">
                                        <label for="office">Sede</label>
                                        <p class="form-control-static">{{ $job->office->name }}</p>
                                    </div>
                                </div>
                            </div>
                            <!--/.row-->

                            <div class="row">
                                <div class="col-sm-12 {{ $errors->has('status') ? 'has-danger' : '' }}">
                                    <div class="form-group">
                                        <label for="name">Estado</label>
                                        {!! Form::select('status', [
                                            \App\Models\Job::STATUS_INPROGRESS => 'EN EJECUCIÓN',
                                            \App\Models\Job::STATUS_PENDING => 'PENDIENTE DE COTIZACIÓN',
                                        ], null, ['class' => 'form-control', 'placeholder' => '-- Estado --']) !!}
                                        @if ($errors->has('status'))
                                            {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!--/.row-->

                            <div class="row">
                                <div class="col-sm-12 {{ $errors->has('job_type') ? 'has-danger' : '' }}">
                                    <div class="form-group">
                                        <label for="name">Tipo de OT</label>
                                        {!! Form::select('job_type', get_job_types(), null, ['class' => 'form-control']) !!}
                                        @if ($errors->has('job_type'))
                                            {!! $errors->first('job_type', '<span class="help-block">:message</span>') !!}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!--/.row-->

                            <div class="row">
                                <div class="col-sm-12 {{ $errors->has('title') ? 'has-danger' : '' }}">
                                    <div class="form-group">
                                        <label for="name">Título</label>
                                        {!! Form::text('title', null, ['class' => 'form-control']) !!}
                                        @if ($errors->has('title'))
                                            {!! $errors->first('title', '<span class="help-block">:message</span>') !!}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!--/.row-->

                            <div class="row">
                                <div class="col-sm-12 {{ $errors->has('priority') ? 'has-danger' : '' }}">
                                    <div class="form-group">
                                        <label for="name">Prioridad</label>
                                        <p class="form-control-static">
                                            @if ($job->priority === \App\Models\Job::PRIORITY_NORMAL)
                                                Normal
                                            @elseif ($job->priority === \App\Models\Job::PRIORITY_URGENT)
                                                Urgente
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--/.row-->

                            <fieldset>
                                <legend>Descripción</legend>

                                <div class="row">
                                    <div class="col-sm-12 {{ $errors->has('description') ? 'has-danger' : '' }}">
                                        <div class="form-group">
                                            <label for="name">Descripción</label>
                                            {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description', 'placeholder' => 'Descripción']) !!}
                                            @if ($errors->has('description'))
                                                {!! $errors->first('description', '<span class="help-block">:message</span>') !!}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <!--/.row-->

                                @if ($images->count())
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="name">Imágenes</label>
                                                <vue-images :imgs='{!! $images !!}'
                                                            :modalclose="true"
                                                            :showclosebutton="true"
                                                            >
                                                </vue-images>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/.row-->
                                @endif
                            </fieldset>

                            <fieldset>
                                <legend>Planificación</legend>

                                <div class="row">
                                    <div class="col-sm-6 {{ $errors->has('starting_at') ? 'has-danger' : '' }}">
                                        <div class="form-group">
                                            <label for="name">Fecha de Inicio</label>
                                            {!! Form::text('starting_at', null, ['class' => 'form-control', 'placeholder' => '', 'data-toggle' => 'datepicker']) !!}
                                            @if ($errors->has('starting_at'))
                                                {!! $errors->first('starting_at', '<span class="help-block">:message</span>') !!}
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-6 {{ $errors->has('ending_at') ? 'has-danger' : '' }}">
                                        <div class="form-group">
                                            <label for="name">Fecha de Término</label>
                                            {!! Form::text('ending_at', null, ['class' => 'form-control', 'placeholder' => '', 'data-toggle' => 'datepicker']) !!}
                                            @if ($errors->has('ending_at'))
                                                {!! $errors->first('ending_at', '<span class="help-block">:message</span>') !!}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <!--/.row-->
                            </fieldset>

                            <fieldset>
                                <legend>Detalle de Costos</legend>

                                <cost-details :items='{!! json_encode(old('costs', $products)) !!}'></cost-details>
                            </fieldset>


                            <div class="row">
                                <div class="col-sm-6">
                                    <a href="{{ route('jobs') }}">&larr; Volver</a>
                                </div>
                                <div class="col-sm-6 text-right">
                                    {!! Form::button('Guardar Pendiente', ['type' => 'submit', 'class' => 'btn btn-success', 'name' => 'save_mode', 'value' => 'save']) !!}
                                    &nbsp;
                                    {!! Form::button('Guardar e Ingresar', ['type' => 'submit', 'class' => 'btn btn-success', 'name' => 'save_mode', 'value' => 'store']) !!}
                                </div>
                            </div>
                            <!--/.row-->
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
            <!--/.col-->
        </div>
        <!--/.row-->

@endsection

@section('scripts')
    @parent

    <!-- include summernote css/js-->
    <link href="{{ asset('js/vendor/wysiwyg/ui/trumbowyg.min.css') }}" rel="stylesheet">
    <script src="{{ asset('js/vendor/wysiwyg/trumbowyg.min.js') }}"></script>

    <!-- include datepicker css/js -->
    <link href="{{ asset('js/vendor/datepicker/dist/datepicker.min.css') }}" rel="stylesheet">
    <script src="{{ asset('js/vendor/datepicker/dist/datepicker.min.js') }}"></script>
@endsection

@push('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#description').trumbowyg();

            $('[data-toggle="datepicker"]').datepicker({
                language: 'es',
                format: 'dd/mm/yyyy',
                autoHide: true
            });
        });
    </script>
@endpush

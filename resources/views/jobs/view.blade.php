@extends('layouts.app')

@section('breadcrumb')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Inicio</li>
        <li class="breadcrumb-item">Orden de Trabajo</li>
        <li class="breadcrumb-item">Ver</li>
        <li class="breadcrumb-item active">#{{ $job->id }} - {{ $job->created_at->format('d/m/Y H:i:s') }}</li>
    </ol>
@endsection

@section('content')
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Orden de Trabajo #{{ $job->id }}</strong> -
                        <small>Ver</small>
                    </div>
                    <div class="card-block">

                        <div class="row">
                            <div class="col-md-2">
                                <label for="office"><strong>Sede:</strong></label>
                            </div>
                            <div class="col-md-10">
                                {{ $job->office->name }}
                            </div>
                        </div>
                        <!--/.row-->

                        <div class="row">
                            <div class="col-md-2">
                                <label for="name"><strong>Estado:</strong></label>
                            </div>
                            <div class="col-md-10">
                                @if ((int) $job->status === \App\Models\Job::STATUS_INPROGRESS)
                                    <span class="badge badge-default">EN EJECUCIÓN</span>
                                @elseif ((int) $job->status === \App\Models\Job::STATUS_PENDING)
                                    <span class="badge badge-primary">PENDIENTE DE COTIZACIÓN</span>
                                @elseif ((int) $job->status === \App\Models\Job::STATUS_COMPLETED)
                                    <span class="badge badge-warning">EJECUTADA</span>
                                @elseif ((int) $job->status === \App\Models\Job::STATUS_REJECTED)
                                    <span class="badge badge-danger">RECHAZADA</span>
                                @elseif ((int) $job->status === \App\Models\Job::STATUS_RECEIVED)
                                    <span class="badge badge-success">APROBADA</span>
                                @endif
                            </div>
                        </div>
                        <!--/.row-->

                        <div class="row">
                            <div class="col-md-2">
                                <label for="name"><strong>Prioridad:</strong></label>
                            </div>
                            <div class="col-md-10" id="priority-container">
                                @if (user()->can('update-job-priority', $job))
                                    {!! Form::select('priority', priorities_collection()->pluck('name', 'id'), $job->priority, ['class' => 'form-control', 'id' => 'switch-priority']) !!}
                                @else
                                    @if ($job->priority === \App\Models\Job::PRIORITY_NORMAL)
                                        <span class="label label-default">Normal</span>
                                    @elseif ($job->priority === \App\Models\Job::PRIORITY_URGENT)
                                        <span class="label label-danger">Urgente</span>
                                    @endif
                                @endif
                            </div>
                        </div>
                        <!--/.row-->

                        <div class="row">
                            <div class="col-md-2">
                                <label for=""><strong>Tipo de OT:</strong></label>
                            </div>
                            <div class="col-md-10">
                                {{ get_job_types()[$job->job_type] }}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <strong>Descripción</strong>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-2">
                                <label for="name"><strong>Título:</strong></label>
                            </div>
                            <div class="col-md-10">
                                {!! $job->title !!}
                            </div>
                        </div>
                        <!--/.row-->

                        <div class="row">
                            <div class="col-md-2">
                                <label for="name"><strong>Descripción:</strong></label>
                            </div>
                            <div class="col-md-10">
                                {!! $job->description !!}
                            </div>
                        </div>
                        <!--/.row-->

                        <div class="row">
                            <div class="col-md-12">
                                <label for="name"><strong>Imágenes:</strong></label>
                            </div>
                            <div class="col-md-12">
                                @if (count($images))
                                    <vue-images :imgs='{!! $images !!}'
                                                :modalclose="true"
                                                :showclosebutton="true"
                                                :showcaption="false"
                                                :imagecountseparator="' de '">
                                    </vue-images>
                                @else
                                    <p class="text-center">No se agregaron imágenes para esta orden</p>
                                @endif
                            </div>
                        </div>
                        <!--/.row-->
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <strong>Planificación</strong>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-2">
                                <label for="name"><strong>Fecha de Inicio:</strong></label>
                            </div>
                            <div class="col-md-4">
                                <p class="form-control-static">{{ $job->starting_at->format('d/m/Y') }}</p>
                            </div>
                            <div class="col-md-2">
                                <label for="name"><strong>Fecha de Término:</strong></label>
                            </div>
                            <div class="col-md-4">
                                @if (!empty($job->ended_at))
                                    {{ $job->ended_at->format('d/m/Y') }}
                                @else
                                    {{ $job->ending_at->format('d/m/Y') }}
                                @endif
                            </div>
                        </div>
                        <!--/.row-->
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <strong>Detalle de Costos</strong>
                    </div>
                    <div class="card-block">
                        <div id="accordion" role="tablist" aria-multiselectable="false" class="d-lg-none">
                            @foreach ($costs as $cost)
                                <div class="card">
                                    <div class="card-header" role="tab" id="heading{{ $loop->index }}">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $loop->index }}" aria-controls="collapse{{ $loop->index }}">
                                            @if ((int) $cost->role == 5)
                                                Estandar - <strong>${{ number_format($cost->pivot->total, 0, ',', '.') }}</strong>
                                            @elseif ((int) $cost->role == 4)
                                                Otro - <strong>${{ number_format($cost->pivot->total, 0, ',', '.') }}</strong>
                                            @elseif ((int) $cost->role == 3)
                                                {{ $cost->description }} - <strong>${{ number_format($cost->pivot->total, 0, ',', '.') }}</strong>
                                            @else
                                                {{ $cost->type }} - <strong>${{ number_format($cost->pivot->total, 0, ',', '.') }}</strong>
                                            @endif
                                        </a>
                                    </div>
                                    <div id="collapse{{ $loop->index }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{ $loop->index }}">
                                        <div class="card-block">
                                            <dl class="row">
                                                <dt class="col-sm-3">Tipo</dt>
                                                @if ((int) $cost->role == 5)
                                                    <dd class="col-sm-9">Estandar</dd>
                                                @elseif ((int) $cost->role == 3)
                                                    <dd class="col-sm-9">Cotización</dd>
                                                @else
                                                    <dd class="col-sm-9">{{ $cost->type }}</dd>
                                                @endif
                                                <dt class="col-sm-3">Producto</dt>
                                                @if ((int) $cost->role == 3)
                                                    <dd class="col-sm-9">{{ $cost->description }}</dd>
                                                @else
                                                    <dd class="col-sm-9">{{ $cost->product }}</dd>
                                                @endif
                                                <dt class="col-sm-3">Cantidad</dt>
                                                <dd class="col-sm-9">{{ $cost->amount }}</dd>
                                                <dt class="col-sm-3">Costo</dt>
                                                <dd class="col-sm-9">${{ number_format($cost->cost, 0, ',', '.') }}</dd>
                                                <dt class="col-sm-3">Total</dt>
                                                <dd class="col-sm-9"><strong>${{ number_format($cost->pivot->total, 0, ',', '.') }}</strong></dd>
                                            </dl>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <table class="table table-collapse d-md-down-none">
                            <thead>
                            <tr>
                                <th width="25%"></th>
                                <th width="25%"></th>
                                <th width="25%"></th>
                                <th width="8.3%">Cantidad</th>
                                <th width="8.3%">Costo</th>
                                <th width="8.3%">Total</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach ($costs as $cost)
                                @if ((int) $cost->role === 1)
                                    <tr>
                                        <td width="25%">{{ $cost->type }}</td>
                                        <td width="50%" colspan="2">{{ $cost->product }}</td>
                                        <td width="8.3%">{{ $cost->amount }}</td>
                                        <td width="8.3%">${{ number_format($cost->cost, 0, ',', '.') }}</td>
                                        <td width="8.3%"><strong>${{ number_format($cost->pivot->total, 0, ',', '.') }}</strong></td>
                                    </tr>
                                @elseif ((int) $cost->role === 2)
                                    <tr>
                                        <td width="75%" colspan="3">{{ $cost->type }}</td>
                                        <td width="8.3%">{{ $cost->amount  }}</td>
                                        <td width="8.3%">${{ number_format($cost->cost, 0, ',', '.') }}</td>
                                        <td width="8.3%"><strong>${{ number_format($cost->pivot->total, 0, ',', '.') }}</strong></td>
                                    </tr>
                                @elseif ((int) $cost->role === 3)
                                    <tr>
                                        <td width="50%" colspan="2">{{ $cost->description }}</td>
                                        <td width="25%">
                                            @if (!empty($cost->file_fullurl))
                                                <a href="{{ $cost->file_fullurl  }}" target="_blank"><i class="fa fa-download"></i></a>
                                            @endif
                                        </td>
                                        <td width="8.3%">--</td>
                                        <td width="8.3%">${{ number_format($cost->cost, 0, ',', '.') }}</td>
                                        <td width="8.3%"><strong>${{ number_format($cost->pivot->total, 0, ',', '.') }}</strong></td>
                                    </tr>
                                @elseif ((int) $cost->role === 4)
                                    <tr>
                                        <td width="25%">{{ $cost->type }}</td>
                                        <td width="50%" colspan="2">{{ $cost->product }}</td>
                                        <td width="8.3%">{{ $cost->amount }}</td>
                                        <td width="8.3%">${{ number_format($cost->cost, 0, ',', '.') }}</td>
                                        <td width="8.3%"><strong>${{ number_format($cost->pivot->total, 0, ',', '.') }}</strong></td>
                                    </tr>
                                @elseif ((int) $cost->role === 5)
                                    <tr>
                                        <td width="25%">Estandar</td>
                                        <td width="50%" colspan="2">{{ $cost->product }}</td>
                                        <td width="8.3%">--</td>
                                        <td width="8.3%">${{ number_format($cost->pivot->total, 0, ',', '.') }}</td>
                                        <td width="8.3%"><strong>${{ number_format($cost->pivot->total, 0, ',', '.') }}</strong></td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="{{ route('jobs') }}">&larr; Volver</a>
                            </div>
                            <div class="col-sm-6 text-right">
                                @if ((int) $job->status === \App\Models\Job::STATUS_INPROGRESS)
                                    @can('complete', $job)
                                        <button type="button" class="btn btn-success" id="completeJob">Finalizar OT</button>
                                    @endcan
                                @endif

                                @if ((int) $job->status === \App\Models\Job::STATUS_COMPLETED)
                                    @can('rate', $job)
                                        <button type="button" class="btn btn-success" id="approveJob">Aprovar OT</button>
                                        <button type="button" class="btn btn-success" id="rejectJob">Rechazar OT</button>
                                    @endcan
                                @endif
                            </div>
                        </div>
                        <!--/.row-->
                    </div>
                </div>
            </div>
            <!--/.col-->
        </div>
        <!--/.row-->

@endsection

@push('inline-scripts')
    <script type="text/javascript">
        $(function() {
            let prioritySwitch = $('#switch-priority'),
                priorityContainer = $('#priority-container'),
                priorityOldValue = prioritySwitch.val();

            prioritySwitch.on('change', function ($event) {
                //$event.preventDefault();
                swal({
                    title: "Esta seguro?",
                    text: "No podrá modificar la prioridad una vez actualizada!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si, cambiar a Urgente!",
                    cancelButtonText: "No, ahora no!",
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    showLoaderOnConfirm: true
                }, function(isConfirm){
                    if (isConfirm) {
                        $.ajax({
                            url: laroute.route('jobs.update.priority', {job: {{ $job->id  }} }),
                            dataType: 'json',
                            method: 'post',
                            data: {
                                priority: prioritySwitch.val()
                            }
                        }).then(function(response) {
                            if (response.success) {
                                priorityOldValue = prioritySwitch.val();
                                priorityContainer.html(response.html);
                                swal("Prioridad actualizada!", "La prioridad de la OT ha sido modificada.", "success");
                            } else {
                                $event.preventDefault();
                                prioritySwitch.val(priorityOldValue);
                                swal("Acción cancelada!", response.message, "error");
                                return false;
                            }
                        }, function () {
                            $event.preventDefault();
                            prioritySwitch.val(priorityOldValue);
                            swal("Acción cancelada!", "Se ha producido un error y la prioridad de la OT no ha sido modificada.", "error");
                            return false;
                        });
                    } else {
                        prioritySwitch.val(priorityOldValue);
                        swal("Acción cancelada!", "La prioridad de la OT no ha sido modificada.", "error");
                        $event.preventDefault();
                        return false;
                    }
                });
            });

            $('#completeJob').on('click', function ($event) {
                swal({
                    title: "Finalizar OT",
                    text: "Está seguro que desa finalizar la OT, el cliente será notificado de esta acción!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si, Finalizar OT!",
                    cancelButtonText: "No, aún no!",
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    showLoaderOnConfirm: true
                }, function(isConfirm){
                    if (isConfirm) {
                        $.ajax({
                            url: laroute.route('jobs.complete', {job: {{ $job->id  }} }),
                            dataType: 'json',
                            method: 'post',
                        }).then(function(response) {
                            if (response.success) {
                                $('#completeJob').remove();
                                swal("OT Finalizada!", "La OT ha sido marcada como finalizada.", "success");
                            } else {
                                $event.preventDefault();
                                swal("Acción cancelada!", response.message, "error");
                                return false;
                            }
                        }, function () {
                            $event.preventDefault();
                            swal("Acción cancelada!", "Se ha producido un error y la OT no ha sido modificada.", "error");
                            return false;
                        });
                    } else {
                        swal("Acción cancelada!", "La OT no ha sido finalizada.", "error");
                        $event.preventDefault();
                        return false;
                    }
                });
            });

            $('#approveJob').on('click', function ($event) {
                swal({
                    title: "Aprovar OT",
                    text: "Está seguro que desea aprobar la OT!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si, Aprobar OT!",
                    cancelButtonText: "No, aún no!",
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    showLoaderOnConfirm: true
                }, function(isConfirm){
                    if (isConfirm) {
                        $.ajax({
                            url: laroute.route('jobs.rate', {job: {{ $job->id  }} }),
                            dataType: 'json',
                            method: 'post',
                            data: {
                                do: 'approve'
                            }
                        }).then(function(response) {
                            if (response.success) {
                                $('#approveJob').remove();
                                $('#rejectJob').remove();
                                swal("OT Aprobada!", "La OT ha sido marcada como aprobada.", "success");
                            } else {
                                $event.preventDefault();
                                swal("Acción cancelada!", response.message, "error");
                                return false;
                            }
                        }, function () {
                            $event.preventDefault();
                            swal("Acción cancelada!", "Se ha producido un error y la OT no ha sido aprobada.", "error");
                            return false;
                        });
                    } else {
                        swal("Acción cancelada!", "La OT no ha sido aprobada.", "error");
                        $event.preventDefault();
                        return false;
                    }
                });
            });

            $('#rejectJob').on('click', function ($event) {
                swal({
                    title: "Aprovar OT",
                    text: "Está seguro que desea rechazar la OT!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si, Rechazar OT!",
                    cancelButtonText: "No, aún no!",
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    showLoaderOnConfirm: true
                }, function(isConfirm){
                    if (isConfirm) {
                        $.ajax({
                            url: laroute.route('jobs.rate', {job: {{ $job->id  }} }),
                            dataType: 'json',
                            method: 'post',
                            data: {
                                do: 'reject'
                            }
                        }).then(function(response) {
                            if (response.success) {
                                $('#approveJob').remove();
                                $('#rejectJob').remove();
                                swal("OT Rechazada!", "La OT ha sido marcada como rechazada.", "success");
                            } else {
                                $event.preventDefault();
                                swal("Acción cancelada!", response.message, "error");
                                return false;
                            }
                        }, function () {
                            $event.preventDefault();
                            swal("Acción cancelada!", "Se ha producido un error y la OT no ha sido rechazada.", "error");
                            return false;
                        });
                    } else {
                        swal("Acción cancelada!", "La OT no ha sido rechazada.", "error");
                        $event.preventDefault();
                        return false;
                    }
                });
            });
        });
    </script>
@endpush

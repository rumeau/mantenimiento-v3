@extends('layouts.app')

@section('breadcrumb')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Inicio</li>
        <li class="breadcrumb-item active">Selección de Cliente</li>
    </ol>
@endsection

@section('content')
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Selección de Cliente
                    </div>
                    <div class="card-block">
                        {!! Form::open(['route' => 'select.client.post']) !!}

                        <div class="row">
                            <div class="col-sm-6 {{ $errors->has('client') ? 'has-danger' : '' }}">
                                <div class="form-group">
                                    @if (!user()->isRole('client'))
                                        {!! Form::select('client', \App\Models\Client::pluck('name', 'id'), null, ['class' => 'form-control', 'placeholder' => '-- Seleccione un Cliente']) !!}
                                    @else
                                        {!! Form::select('client', user()->clients->pluck('name', 'id'), null, ['class' => 'form-control', 'placeholder' => '-- Seleccione un Cliente']) !!}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary">Seleccionar &rarr;</button>
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--/.row-->

@endsection
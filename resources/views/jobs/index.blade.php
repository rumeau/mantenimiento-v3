@extends('layouts.app')

@section('breadcrumb')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Inicio</li>
        <li class="breadcrumb-item active">Órdenes de Trabajo</li>
    </ol>
@endsection

@section('content')
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-5">
                                <h4 class="card-title"><small><i class="fa fa-align-justify"></i></small> Órdenes de Trabajo</h4>
                            </div>
                            @can('create', \App\Models\Job::class)
                                <div class="col-sm-7">
                                    <div class="btn-toolbar pull-right" role="toolbar" aria-label="Toolbar with button groups">
                                        <div class="btn-group hidden-sm-down" role="group" aria-label="Second group">
                                            <a class="btn btn-primary" href="{{ route('jobs.create') }}"><i class="icon-plus"></i> &nbsp;Nueva OT</a>
                                        </div>
                                    </div>
                                </div>
                            @endcan
                        </div>

                        <div id="jsGrid"></div>
                    </div>
                </div>
            </div>
        </div>
        <!--/.row-->
@endsection

@section('scripts')
    @parent

    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.css" />
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid-theme.min.css" />

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.js"></script>
@endsection

@push('inline-scripts')
<script type="text/javascript">
    $("#jsGrid").jsGrid({
        height: "auto",
        width: "100%",

        sorting: true,
        autoload: true,
        filtering: true,
        paging: true,
        pageLoading: true,
        pageSize: 15,
        pageIndex: 1,

        pagerFormat: "Páginas: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} de {pageCount}",
        pagePrevText: "Anterior",
        pageNextText: "Siguiente",
        pageFirstText: "Primera",
        pageLastText: "Ultima",
        pageNavigatorNextText: "...",
        pageNavigatorPrevText: "...",
        pagerRenderer: null,

        controller: {
            loadData: function(filter) {
                return $.ajax({
                    url: laroute.route('jobs'),
                    dataType: "json",
                    data: {
                        page: filter.pageIndex,
                        limit: filter.pageSize,
                        filter: filter
                    }
                });
            }
        },

        fields: [
            { name: "id", title: "#", type: "text", width: 55, filtering: false },
            { name: "title", title: "Título", type: "text", filtering: true, css: "hidden-sm hidden-md hidden-smd hidden-lg", itemTemplate: function (value, item) {
                if (value !== null) {
                    return value.length > 25 ? value.substring(0, 25) + "..." : value;
                }

                return '';
            } },
            { name: "viewed", css: "hidden-sm hidden-md hidden-smd hidden-lg text-center", title: "Revisado", type: "text", filtering: false, sorting: false, width: 75, itemTemplate: function (value) {
                if (value === 1) {
                    return '<i class="fa fa-eye" title="Visto"></i>';
                } else if (value === 2) {
                    return '<i class="fa fa-eye" title="Visto"></i>&nbsp;<i class="fa fa-check" title="Revisado"></i>';
                }

                return '';
            } },
            { name: "priority", title: "Prioridad", type: "select", width: 100, items: {!! priority_filter_items() !!}, valueField: 'id', textField: 'name', selectedIndex: 0, itemTemplate : function (value, item) {
                if (item.priority === {{ \App\Models\Job::PRIORITY_URGENT }}) {
                    return '<span class="tag tag-danger">Urgente</span>';
                }

                return '<span class="tag tag-default">Normal</span>';
            }},
            { name: "author.name", css: "hidden-sm hidden-md hidden-smd hidden-lg", title: "Usuario", type: 'text', width: '100%', filtering: true },
            { name: "office_id", css: "hidden-sm hidden-smd hidden-lg", title: "Sede", type: 'select', width: 150, filtering: true, items: {!! user_offices_filter_items() !!}, valueField: 'id', textField: 'name', selectedIndex: 0 },
            { name: "create_date", css: "hidden-sm hidden-smd", title: "F. Registro", width: 100, type: 'text', filtering: false, itemTemplate: function (value, item) {
                return moment(item.created_at).format('DD/MM/YYYY');
            } },
            { name: "start_date", css: "hidden-sm hidden-smd", title: "F. Inicio", width: 100, type: 'text', filtering: false, itemTemplate: function (value, item) {
                let start_date = _.isEmpty(item.started_at) ? item.starting_at : item.started_at;
                return moment(start_date).format('DD/MM/YYYY');
            } },
            { name: "end_date", css: "hidden-sm", title: "F.Término", width: 100, type: 'text', filtering: false, itemTemplate: function (value, item) {
                let end_date = _.isEmpty(item.ended_at) ? item.ending_at : item.ended_at;
                return moment(end_date).format('DD/MM/YYYY');
            } },
            { name: "status", css: "hidden-sm hidden-smd", title: "Estado", width: 120, type: 'select', items: {!! job_status_filter_items() !!}, valueField: 'id', textField: 'name', selectedIndex: 0, itemTemplate: function (value) {
                if (value === {{ \App\Models\Job::STATUS_PENDING }}) {
                    return '<span class="badge badge-primary" style="white-space: normal;">PENDIENTE DE COTIZACIÓN</span>';
                } else if (value === {{ \App\Models\Job::STATUS_COMPLETED }}) {
                    return '<span class="badge badge-warning">EJECUTADA</span>';
                } else if (value === {{ \App\Models\Job::STATUS_RECEIVED }}) {
                    return '<span class="badge badge-success">APROBADA</span>';
                } else if (value === {{ \App\Models\Job::STATUS_REJECTED }}) {
                    return '<span class="badge badge-danger">RECHAZADA</span>';
                }

                return '<span class="badge badge-default">EN EJECUCIÓN</span>';
            }},
            { width: 100, align: "center", sorting: false,
                itemTemplate: function(value, item) {
                    let html = '';
                    if (item.status === {{ \App\Models\Job::STATUS_PENDING }} && {{ user()->group() != 'default' ? 'true' : 'false'  }}) {
                        html += '<a href="' + laroute.route('jobs.edit', {job: item.id}) + '">Editar</a>';
                    } else {
                        html += '<a href="' + laroute.route('jobs.view', {job: item.id}) + '">Ver</a>';
                    }

                    return html;
                }
            }
        ]
    });

    $( window ).resize(function() {
        $("#jsGrid").jsGrid('render');
    });
</script>
@endpush

@extends('layouts.app')

@section('breadcrumb')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Inicio</li>
        <li class="breadcrumb-item active">Usuarios</li>
    </ol>
@endsection

@section('content')

    <div class="animated fadeIn">


        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">

                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-5">
                                <h4 class="card-title"><small><i class="fa fa-align-justify"></i></small> Usuarios</h4>
                            </div>
                            <div class="col-sm-7">
                                <div class="btn-toolbar pull-right" role="toolbar" aria-label="Toolbar with button groups">
                                    <div class="btn-group hidden-sm-down" role="group" aria-label="Second group">
                                        <a class="btn btn-primary" href="{{ route('users.create') }}"><i class="icon-plus"></i> &nbsp;Nuevo Usuario</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="offset-md-6 col-md-6 offset-lg-8 col-lg-4 text-right">
                                {!! Form::select('client', clients_array_select(), null, ['class' => 'form-control', 'placeholder' => '-- Cliente --', 'id' => 'clientFilter']) !!}
                            </div>
                        </div>

                        <div id="jsGrid"></div>
                    </div>
                </div>
            </div>
        </div>
        <!--/.row-->
    </div>

@endsection

@section('scripts')
    @parent

    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.css" />
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid-theme.min.css" />

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.js"></script>
@endsection

@push('inline-scripts')
<script type="text/javascript">
    $("#jsGrid").jsGrid({
        height: "auto",
        width: "100%",

        filtering: true,
        sorting: true,
        autoload: true,
        paging: true,
        pageLoading: true,
        pageSize: 20,
        pageIndex: 1,

        pagerFormat: "Páginas: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} de {pageCount}",
        pagePrevText: "Anterior",
        pageNextText: "Siguiente",
        pageFirstText: "Primera",
        pageLastText: "Ultima",
        pageNavigatorNextText: "...",
        pageNavigatorPrevText: "...",
        pagerRenderer: null,

        controller: {
            loadData: function(filter) {
                return $.ajax({
                    url: laroute.route('users'),
                    dataType: "json",
                    data: {
                        page: filter.pageIndex,
                        limit: filter.pageSize,
                        filter: filter
                    }
                });
            }
        },

        fields: [
            { name: "id", title: "#", type: "text", css: "hidden-sm hidden-smd", width: 4, filtering: false },
            { name: "name", title: "Nombre", type: "text", filtering: false },
            { name: "role_name", title: "Tipo de Usuario", css: "hidden-sm hidden-smd", width: 55, type: "select", filtering: true, items: {!! roles_filter_items() !!}, valueField: 'id', textField: 'name', valueType: "text",
                itemTemplate: function (value) {
                    return value;
                }
            },
            /*
            { name: "client", title: "Cliente", width: 30, type: "select", filtering: true, items: {!! user_clients_filter_items() !!}, valueField: 'id', textField: 'name',
                itemTemplate: function (value) {
                    return value.name;
                }
            },*/
            { name: "email", title: "E-mail", width: 55, type: "text", css: "hidden-sm hidden-smd hidden-md", sorting: false, filtering: false },
            { name: "status", title: "Estado", width: 10, type: "select", css: "hidden-sm hidden-smd hidden-md hidden-lg", sorting: false, filtering: true, items: {!! user_status_filter_items() !!}, valueField: 'id', textField: 'name', selectedIndex: 2,
                itemTemplate: function (value) {
                    if (value) {
                        return '<i class="fa fa-check"></i>';
                    } else {
                        return '<i class="fa fa-times"></i>';
                    }
                }
            },
            { width: 117, align: "center", sorting: false, filtering: false,
                itemTemplate: function(value, item) {
                    if (item.trashed) {
                        return '<a href="' + laroute.route('users.restore', {user: item.id}) + '">Restaurar</a>';
                    } else {
                        return '<a href="' + laroute.route('users.edit', {user: item.id}) + '">Editar</a> | <a href="' + laroute.route('users.delete', {user: item.id}) + '">Eliminar</a>';
                    }
                }
            }
        ]
    });

    $('#clientFilter').change(function() {
        $("#jsGrid").jsGrid("loadData", { client_id: $(this).val() });
    });
</script>
@endpush
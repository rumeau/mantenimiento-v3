@extends('layouts.app')

@section('breadcrumb')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Inicio</li>
        <li class="breadcrumb-item">Usuarios</li>
        <li class="breadcrumb-item">Eliminar</li>
        <li class="breadcrumb-item active">{{ $user->name }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <strong>Usuario</strong>
                    <small>Eliminar</small>
                </div>
                <div class="card-block">

                    {!! Form::open(['route' => ['users.destroy', $user]]) !!}

                        {!! Form::hidden('user_id', $user->id) !!}
                    <div class="row">
                        <p>Está seguro que desea eliminar el usuario con nombre <strong>"{{ $user->name }}"</strong></p>
                    </div>
                    <!--/.row-->

                    <div class="row">
                        <div class="col-sm-6">
                            <a href="{{ route('users') }}">&larr; Volver</a>
                        </div>
                        <div class="col-sm-6 text-right">
                            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
                        </div>
                    </div>
                    <!--/.row-->
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
        <!--/.col-->
    </div>
    <!--/.row-->
@endsection
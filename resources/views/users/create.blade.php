@extends('layouts.app')

@section('breadcrumb')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Inicio</li>
        <li class="breadcrumb-item">Usuarios</li>
        <li class="breadcrumb-item active">Crear</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <strong>Usuario</strong>
                    <small>Crear</small>
                </div>
                <div class="card-block">

                    {!! Form::open(['route' => 'users.store']) !!}

                        <div class="row">
                            <div class="col-sm-12 {{ $errors->has('name') ? 'has-danger' : '' }}">
                                <div class="form-group">
                                    <label for="name">Nombre</label>
                                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) !!}
                                    @if ($errors->has('name'))
                                        {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!--/.row-->

                        <div class="row">
                            <div class="col-sm-12 {{ $errors->has('email') ? 'has-danger' : '' }}">
                                <div class="form-group">
                                    <label for="name">E-mail</label>
                                    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'E-mail']) !!}
                                    @if ($errors->has('email'))
                                        {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!--/.row-->

                        <div class="row">
                            <div class="col-sm-12 {{ $errors->has('roles') ? 'has-danger' : '' }}">
                                <div class="form-group">
                                    <label for="name">Tipo de Usuario</label>
                                    {!! Form::select('roles', $roles->pluck('name', 'slug'), null, ['class' => 'form-control', 'placeholder' => 'Tipo de Usuario', 'id' => 'role']) !!}
                                    @if ($errors->has('roles'))
                                        {!! $errors->first('roles', '<span class="help-block">:message</span>') !!}
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!--/.row-->

                        <div id="officeDiv" style="display: none;">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="client">Cliente</label>
                                        {!! Form::select('client', \App\Models\Client::pluck('name', 'id'), null, ['class' => 'form-control', 'id' => 'client', 'size' => 15, 'placeholder' => '-- Cliente --']) !!}
                                    </div>
                                </div>
                                <div class="col-sm-6 {{ $errors->has('offices') ? 'has-danger' : '' }}">
                                    <div class="form-group">
                                        <label for="name">Clientes</label>
                                        {!! Form::select('offices[]', clients_offices_array_select(), null, ['class' => 'form-control', 'multiple' => true, 'id' => 'office', 'size' => 15]) !!}
                                        @if ($errors->has('offices'))
                                            {!! $errors->first('offices', '<span class="help-block">:message</span>') !!}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!--/.row-->
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <a href="{{ route('users') }}">&larr; Volver</a>
                            </div>
                            <div class="col-sm-6 text-right">
                                {!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
                            </div>
                        </div>
                        <!--/.row-->
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
        <!--/.col-->
    </div>
    <!--/.row-->
@endsection

@push('inline-scripts')
<script type="text/javascript">
    $(function() {
        let officeDiv = $('#officeDiv'),
            roleDiv = $('#role'),
            client = $('#client'),
            office = $('#office');

        roleDiv.on('change', function() {
            let el = $(this);
            if (el.val() === 'client') {
                officeDiv.show();
            } else {
                officeDiv.hide();
            }
        });

        roleDiv.trigger('change');

        office.find('optgroup').hide();
        client.on('change', function () {
            let clientVal = $(this).val(),
                clientSelected = $(this).find('option[value="' + clientVal + '"]');

            office.find('optgroup').hide();
            office.find('option:selected').removeAttr("selected");
            office.find('option[value^="' + clientVal + '_"]').closest('optgroup').show();
        });

        client.trigger('change');
        @if (!empty(old('client', '')))
            client.val({!! old('client', '') !!});
            @if (!empty(old('offices', [])))
                office.val({!! json_encode(old('offices', [])) !!});
            @endif
        @endif
    });
</script>
@endpush
@extends('layouts.app')

@section('breadcrumb')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Inicio</li>
        <li class="breadcrumb-item active">Configuración</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <strong>Configuración</strong>
                </div>
                <div class="card-block">

                    {!! Form::open(['route' => 'settings.save']) !!}

                    <div class="row">
                        <div class="col-sm-12 {{ $errors->has('priority_tta') ? 'has-danger' : '' }}">
                            <div class="form-group">
                                <label for="name">Tiempo para asignar Urgencia (en minutos)</label>
                                {!! Form::number('priority_tta', configger()->get('priority_tta', 120), ['class' => 'form-control', 'placeholder' => '120']) !!}
                                @if ($errors->has('priority_tta'))
                                    {!! $errors->first('priority_tta', '<span class="help-block">:message</span>') !!}
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--/.row-->

                    <div class="row">
                        <div class="col-sm-12 {{ $errors->has('session_ttl') ? 'has-danger' : '' }}">
                            <div class="form-group">
                                <label for="name">Duración de la sesión (en minutos)</label>
                                {!! Form::number('session_ttl', configger()->get('session_ttl', 120), ['class' => 'form-control', 'placeholder' => '120']) !!}
                                @if ($errors->has('session_ttl'))
                                    {!! $errors->first('session_ttl', '<span class="help-block">:message</span>') !!}
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--/.row-->

                    <div class="row">
                        <div class="col-sm-12 {{ $errors->has('password_strength') ? 'has-danger' : '' }}">
                            <div class="form-group">
                                <label for="name">Complejidad de la Clave (para nuevos registros y cambios)</label>
                                {!! Form::select('password_strength', password_strength_combo(), configger()->get('password_strength', 'basic'), ['class' => 'form-control']) !!}
                                @if ($errors->has('password_strength'))
                                    {!! $errors->first('password_strength', '<span class="help-block">:message</span>') !!}
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--/.row-->

                    <div class="row">
                        <div class="col-sm-12 {{ $errors->has('password_length') ? 'has-danger' : '' }}">
                            <div class="form-group">
                                <label for="name">Largo mínimo de la clave</label>
                                {!! Form::number('password_length', configger()->get('password_length', 6), ['class' => 'form-control']) !!}
                                @if ($errors->has('password_length'))
                                    {!! $errors->first('password_length', '<span class="help-block">:message</span>') !!}
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--/.row-->

                    <div class="row">
                        <div class="col-sm-12 {{ $errors->has('job_types') ? 'has-danger' : '' }}">
                            <div class="form-group">
                                <label for="name">Tipos de OT (Separados por una ,)</label>
                                {!! Form::text('job_types', configger()->get('job_types'), ['class' => 'form-control', 'data-role' => 'tagsinput']) !!}
                                @if ($errors->has('job_types'))
                                    {!! $errors->first('job_types', '<span class="help-block">:message</span>') !!}
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--/.row-->

                    <div class="row">
                        <div class="col-sm-12">
                            {!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                    <!--/.row-->
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
        <!--/.col-->
    </div>
    <!--/.row-->

@endsection

@section('scripts')
    @parent

    <link href="{{ asset('js/vendor/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <script src="{{ asset('js/vendor/bootstrap-tagsinput.min.js') }}"></script>

@endsection

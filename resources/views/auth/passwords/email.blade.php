@extends('layouts.auth')

@section('content')
    <div class="container">
        <div class="text-center">
            <img src="{{ asset('img/logo.png') }}" title="" />
            <br /><br />
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card-group mb-0">
                    <div class="card p-2">
                        <div class="card-block">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            {!! Form::open(['route' => 'password.email', 'class' => 'form-vertical']) !!}

                                <p class="text-muted">Ingrese su dirección de email y le enviaremos instrucciones para recuperar su clave.</p>
                                <div class="input-group mb-1 {{ $errors->has('email') ? ' has-danger' : '' }}">
                                    <span class="input-group-addon"><i class="icon-user"></i></span>
                                    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'E-mail']) !!}
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <a href="{{ url('login') }}" title="Volver" class="btn btn-link px-0">&larr; Volver</a>
                                    </div>
                                    <div class="col-6 text-right">
                                        {!! Form::button('Recuperar', ['type' => 'submit', 'class' => 'btn btn-primary px-2']) !!}
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@extends('layouts.auth')

@section('content')
    <div class="container">
        <div class="text-center">
            <img src="{{ asset('img/logo.png') }}" title="" />
            <br /><br />
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6">
                @include('flash::messages')

                <div class="card-group mb-0">
                    <div class="card p-2">
                        <div class="card-block">
                            <h1>Reestablecer Clave</h1>

                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif

                            {!! Form::open(['route' => 'password.request', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}

                            {!! Form::hidden('token', $token) !!}

                            <div class="input-group mb-2 {{ $errors->has('email') ? ' has-danger' : '' }}">
                                <span class="input-group-addon"><i class="icon-envelope"></i></span>
                                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'E-mail', 'required', 'autofocus']) !!}
                            </div>

                            <div class="input-group mb-2 {{ $errors->has('password') ? ' has-danger' : '' }}">
                                <span class="input-group-addon"><i class="icon-lock"></i></span>
                                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Clave', 'required']) !!}
                            </div>
                            @if ($errors->has('password'))
                                <div class="row has-danger">
                                    <div class="col-12">
                                        {!! $errors->first('password', '<p>:message</p>') !!}
                                    </div>
                                </div>
                            @endif

                            <div class="input-group mb-2 {{ $errors->has('password_confirmation') ? ' has-danger' : '' }}">
                                <span class="input-group-addon"><i class="icon-lock"></i></span>
                                {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirmar Clave', 'required']) !!}
                            </div>
                            @if ($errors->has('password_confirmation'))
                                <div class="row has-danger">
                                    <div class="col-12">
                                        {!! $errors->first('password', '<p>:message</p>') !!}
                                    </div>
                                </div>
                            @endif

                            <div class="row">
                                <div class="col-6">
                                    {!! Form::button('Reestablecer Clave', ['type' => 'submit', 'class' => 'btn btn-primary px-2']) !!}
                                </div>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

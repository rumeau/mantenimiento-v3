@extends('layouts.auth')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                @include('flash::messages')

                <div class="card mx-2">
                    <div class="card-block p-2">
                        <h1>Registro</h1>
                        <p class="text-muted">Crear una nueva cuenta</p>

                        {!! Form::open(['url' => 'register']) !!}

                            <div class="input-group mb-1 {{ $errors->has('email') ? 'has-danger' : '' }}">
                                <span class="input-group-addon">@</span>
                                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'E-mail']) !!}
                            </div>

                            <div class="input-group mb-1 {{ $errors->has('name') ? 'has-danger': '' }}">
                                <span class="input-group-addon"><i class="icon-user"></i></span>
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) !!}
                            </div>

                            <div class="input-group mb-1 {{ $errors->has('password') ? 'has-danger' : '' }}">
                                <span class="input-group-addon"><i class="icon-lock"></i></span>
                                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Clave']) !!}
                            </div>

                            <div class="input-group mb-2 {{ $errors->has('password_confirmation') ? 'has-danger' : '' }}">
                                <span class="input-group-addon"><i class="icon-lock"></i></span>
                                {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirmar Clave']) !!}
                            </div>

                        {!! Form::button('Crear cuenta', ['type' => 'submit', 'class' => 'btn btn-block btn-success']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

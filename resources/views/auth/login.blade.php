@extends('layouts.auth')

@section('content')
<div class="container">
    <div class="text-center">
        <img src="{{ asset('img/logo.png') }}" title="" />
        <br /><br />
    </div>
    <div class="row justify-content-center">
        <div class="col-md-6">
            @include('flash::messages')

            <div class="card-group mb-0">
                <div class="card p-2">
                    <div class="card-block">
                        <h1>Iniciar Sesión</h1>
                        <p class="text-muted">Ingresar a la aplicación</p>
                        {!! Form::open(['route' => 'login']) !!}

                            <div class="input-group mb-1 {{ $errors->has('email') ? ' has-danger' : '' }}">
                                <span class="input-group-addon"><i class="icon-user"></i></span>
                                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'E-mail']) !!}
                            </div>
                            <div class="input-group mb-2 {{ $errors->has('password') ? ' has-danger' : '' }}">
                                <span class="input-group-addon"><i class="icon-lock"></i></span>
                                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Clave']) !!}
                            </div>
                            <div class="form-check form-check-inline mb-2">
                                <label class="form-check-label">
                                    {!! Form::checkbox('remember', 1, null, ['class' => 'form-check-input']) !!}
                                    Recordarme en este equipo
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    {!! Form::button('Ingresar', ['type' => 'submit', 'class' => 'btn btn-primary px-2']) !!}
                                </div>
                                <div class="col-6 text-right">
                                    <a href="{{ route('password.request') }}" title="Olvidé mi clave?" class="btn btn-link px-0">Olvidé mi clave?</a>
                                </div>
                            </div>
                            @if ($allowRegister)
                                <div class="row">
                                    <div class="text-center">
                                        <a href="{{ url('register') }}" title="Crear una nueva cuenta" class="btn btn-link px-0">Crear una nueva cuenta</a>
                                    </div>
                                </div>
                            @endif
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

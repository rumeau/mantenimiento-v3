<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('home') }}"><i class="icon-speedometer"></i> Dashboard</a>
            </li>
            @if (\Auth::user()->can('create', App\Models\Client::class))
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#"><i class="fa fa-building"></i> Empresas</a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('clients') }}"><i class="fa fa-building"></i> Ver Empresas</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('clients.create') }}"><i class="icon-plus"></i> Nueva Empresa</a>
                        </li>
                    </ul>
                </li>
            @endif

            @if (user()->can('create', App\Models\Office::class))
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#"><i class="fa fa-building"></i> Sedes</a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('offices') }}"><i class="fa fa-building"></i> Ver Sedes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('offices.create') }}"><i class="icon-plus"></i> Nueva Sede</a>
                        </li>
                    </ul>
                </li>
            @endif

            @if (user()->can('create', App\Models\User::class))
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#"><i class="fa fa-user"></i> Usuarios</a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('users') }}"><i class="fa fa-user"></i> Ver Usuarios</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('users.create') }}"><i class="icon-plus"></i> Nuevo Usuario</a>
                        </li>
                    </ul>
                </li>
            @endif

            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#"><i class="fa fa-file"></i> Órdenes de Trabajo</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('jobs') }}"><i class="fa fa-file"></i> Ver OT</a>
                    </li>
                    @if (user()->can('create', App\Models\Job::class))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('jobs.create') }}"><i class="fa fa-plus"></i> Nueva OT</a>
                        </li>
                    @endif
                </ul>
            </li>

            @role('superadmin')
            <li class="nav-item nav-dropdown">
                <a class="nav-link" href="{{ route('settings') }}"><i class="fa fa-gear"></i> Configuración</a>
            </li>
            @endrole

            <li class="nav-item">
                <a class="nav-link" href="{{ route('profile') }}"><i class="icon-user"></i> Mi Perfil</a>
            </li>
        </ul>
    </nav>
</div>

<header class="app-header navbar">
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none" type="button">☰</button>
    <a class="navbar-brand" href="#"></a>
    <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item">
            <a class="nav-link navbar-toggler sidebar-toggler" href="#">☰</a>
        </li>
    </ul>

    @if (Auth::check())
    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item d-md-down-none">
            @if (user()->isRole('client'))
                @if (($clients = user()->clients()->count()) > 0)
                    @if ($clients == 1)
                        <a class="nav-link" href="#">
                            <i class="fa -building"></i>
                            <span class="hidden-md-down">{{ user()->client->name }}</span>
                        </a>
                    @else
                        <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-building"></i>
                            <span class="hidden-md-down">
                                {{ !user()->client ? 'Seleccione un cliente...' : user()->client->name }}
                            </span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="{{ url('select-client') }}"><i class="fa fa-spinner"></i> Cambiar cliente</a>
                        </div>
                    @endif
                @endif
            @else
                <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-building"></i>
                    <span class="hidden-md-down">
                        {{ !user()->client ? 'Seleccione un cliente...' : user()->client->name }}
                    </span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="{{ url('select-client') }}"><i class="fa fa-spinner"></i> Cambiar cliente</a>
                </div>
            @endif
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <img src="{{ user()->avatar }}" class="img-avatar" alt="{{ user()->email }}">
                <span class="d-lg-down-none">{{ user()->name }}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="d-lg-none">
                    <div class="dropdown-header text-center">
                        <strong>Cliente</strong>
                    </div>
                    @if (user()->isRole('client'))
                        @if (($clients = user()->clients()->count()) > 0)
                            @if ($clients == 1)
                                <a class="dropdown-item" href="#"><i class="fa -building"></i> {{ user()->client->name }}</a>
                            @else
                                <a class="dropdown-item" href="#"><i class="fa fa-building"></i>{{ !user()->client ? 'Seleccione un cliente...' : user()->client->name }}</a>
                                <a class="dropdown-item" href="{{ url('select-client') }}"><i class="fa fa-spinner"></i> Cambiar cliente</a>
                            @endif
                        @endif
                    @else
                        <a class="dropdown-item" href="#"><i class="fa fa-building"></i> {{ !user()->client ? 'Seleccione un cliente...' : user()->client->name }}</a>
                        <a class="dropdown-item" href="{{ url('select-client') }}"><i class="fa fa-spinner"></i> Cambiar cliente</a>
                    @endif
                </div>
                <div class="dropdown-header text-center">
                    <strong>Cuenta</strong>
                </div>
                <a class="dropdown-item" href="{{ route('profile') }}"><i class="fa fa-user"></i> Perfil</a>
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                document.getElementById('logout-form').submit();"><i class="fa fa-lock"></i> Cerrar Sesión</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </li>

        <li class="nav-item d-md-down-none">
            &nbsp;
        </li>
    </ul>
        @endif
</header>
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'La clave debe tener al menos 6 caracteres y coincidir con la confirmación.',
    'reset' => '¡Su clave ha sido reestablecida!',
    'sent' => '¡Recordatorio de clave enviado!',
    'token' => 'Este token de reestablecimiento de clave es inválido.',
    'user' => 'No se ha encontrado un usuario con esa dirección de correo.',

    'notification' => [
        'line_1' => 'Esta recibiendo este email por que hemos recibido una solicitud de reestablecimiento de clave para su cuenta.',
        'action' => 'Reestablecer clave',
        'line_2' => 'Si ud. no ha solicitado el reestablecimiento de clave, por favor sólo omita este mensaje.'
    ]
];

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('healthcheck', function() {
    return response()->json([
        'code' => 200,
        'message' => 'Service Up and Running!'
    ]);
});

Auth::routes();

Route::get('/', function() {
    return redirect('/home');
});
Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::group(['middleware' => 'auth'], function () {

    Route::get('/clients', ['as' => 'clients', 'uses' => 'ClientController@index'])->middleware('can:list,App\Models\Client');
    Route::get('/clients/create', ['as' => 'clients.create', 'uses' => 'ClientController@create'])->middleware('can:create,App\Models\Client');
    Route::post('/clients', ['as' => 'clients.store', 'uses' => 'ClientController@store'])->middleware('can:create,App\Models\Client');
    Route::get('/clients/edit/{client}', ['as' => 'clients.edit', 'uses' => 'ClientController@edit'])->middleware('can:update,client');
    Route::post('/clients/edit/{client}', ['as' => 'clients.update', 'uses' => 'ClientController@update'])->middleware('can:update,client');
    Route::get('/clients/delete/{client}', ['as' => 'clients.delete', 'uses' => 'ClientController@delete'])->middleware('can:delete,client');
    Route::post('/clients/destroy', ['as' => 'clients.destroy', 'uses' => 'ClientController@destroy'])->middleware('can:delete,App\Models\Client');
    Route::get('/clients/restore/{client}', ['as' => 'clients.restore', 'uses' => 'ClientController@restore'])->middleware('can:delete,App\Models\Client');

    Route::get('/offices', ['as' => 'offices', 'uses' => 'OfficeController@index']);
    Route::get('/offices/create', ['as' => 'offices.create', 'uses' => 'OfficeController@create'])->middleware('can:create,App\Models\Office');
    Route::post('/offices', ['as' => 'offices.store', 'uses' => 'OfficeController@store'])->middleware('can:create,App\Models\Office');
    Route::get('/offices/edit/{office}', ['as' => 'offices.edit', 'uses' => 'OfficeController@edit'])->middleware('can:update,office');
    Route::post('/offices/edit/{office}', ['as' => 'offices.update', 'uses' => 'OfficeController@update'])->middleware('can:update,office');
    Route::get('/offices/delete/{office}', ['as' => 'offices.delete', 'uses' => 'OfficeController@delete'])->middleware('can:delete,office');
    Route::post('/offices/destroy', ['as' => 'offices.destroy', 'uses' => 'OfficeController@destroy'])->middleware('can:delete,App\Models\Office');
    Route::get('/offices/restore/{office}', ['as' => 'offices.restore', 'uses' => 'OfficeController@restore'])->middleware('can:delete,App\Models\Office');
    Route::get('/offices/{client}/users', ['as' => 'offices.client.users', 'uses' => 'OfficeController@users']);

    Route::get('/users', ['as' => 'users', 'uses' => 'UserController@index'])->middleware('can:list,App\Models\User');
    Route::get('/users/create', ['as' => 'users.create', 'uses' => 'UserController@create'])->middleware('can:create,App\Models\User');
    Route::post('/users', ['as' => 'users.store', 'uses' => 'UserController@store'])->middleware('can:create,App\Models\User');
    Route::get('/users/edit/{user}', ['as' => 'users.edit', 'uses' => 'UserController@edit'])->middleware('can:update,user');
    Route::post('/users/edit/{user}', ['as' => 'users.update', 'uses' => 'UserController@update'])->middleware('can:update,user');
    Route::get('/users/delete/{user}', ['as' => 'users.delete', 'uses' => 'UserController@delete'])->middleware('can:delete,user');
    Route::post('/users/destroy', ['as' => 'users.destroy', 'uses' => 'UserController@destroy'])->middleware('can:delete,App\Models\User');
    Route::get('/users/restore/{user}', ['as' => 'users.restore', 'uses' => 'UserController@restore'])->middleware('can:delete,App\Models\User');

    Route::get('/settings', ['as' => 'settings', 'uses' => 'SettingsController@index']);
    Route::post('/settings', ['as' => 'settings.save', 'uses' => 'SettingsController@save']);

    Route::get('/profile', ['as' => 'profile', 'uses' => 'ProfileController@index']);
    Route::post('/profile', ['as' => 'profile.save', 'uses' => 'ProfileController@save']);

    Route::get('/jobs', ['as' => 'jobs', 'uses' => 'JobController@index']);
    Route::get('/jobs/create', ['as' => 'jobs.create', 'uses' => 'JobController@create'])->middleware('can:create,App\Models\Job');
    Route::post('/jobs', ['as' => 'jobs.store', 'uses' => 'JobController@store'])->middleware('can:create,App\Models\Job');
    Route::get('/jobs/edit/{job}', ['as' => 'jobs.edit', 'uses' => 'JobController@edit'])->middleware('can:update,job');
    Route::post('/jobs/edit/{job}', ['as' => 'jobs.update', 'uses' => 'JobController@update'])->middleware('can:update,job');
    Route::get('/jobs/delete/{job}', ['as' => 'jobs.delete', 'uses' => 'JobController@delete'])->middleware('can:delete,job');
    Route::post('/jobs/destroy', ['as' => 'jobs.destroy', 'uses' => 'JobController@destroy'])->middleware('can:delete,App\Models\Job');
    Route::get('/jobs/restore/{job}', ['as' => 'jobs.restore', 'uses' => 'JobController@restore'])->middleware('can:delete,App\Models\Job');
    Route::get('/jobs/costs/products', ['as' => 'jobs.costs.products', 'uses' => 'JobController@costProducts']);
    Route::get('/jobs/view/{job}', ['as' => 'jobs.view', 'uses' => 'JobController@view'])->middleware('can:view,job');
    Route::post('/jobs/edit-priority/{job}', ['as' => 'jobs.update.priority', 'uses' => 'JobController@updatePriority']);
    Route::post('/jobs/complete/{job}', ['as' => 'jobs.complete', 'uses' => 'JobController@complete']);
    Route::post('/jobs/rate/{job}', ['as' => 'jobs.rate', 'uses' => 'JobController@rate']);

    Route::get('/select-client', ['as' => 'select.client', 'uses' => 'JobController@client']);
    Route::post('/select-client/post', ['as' => 'select.client.post', 'uses' => 'JobController@postClient']);
});


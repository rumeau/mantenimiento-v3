<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status');
            $table->integer('priority');
            $table->text('description')->nullable();
            $table->integer('type')->nullable();
            $table->integer('client_id')->unsigned();
            $table->integer('office_id')->nullable()->unsigned();
            $table->integer('author_id')->unsigned();
            $table->integer('responsible_id')->nullable()->unsigned();

            $table->timestamps();
            $table->timestamp('starting_at')->nullable();
            $table->timestamp('started_at')->nullable();
            $table->timestamp('ending_at')->nullable();
            $table->timestamp('ended_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jobs');
    }
}

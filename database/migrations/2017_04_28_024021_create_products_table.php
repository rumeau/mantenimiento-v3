<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->nullable();
            $table->string('product')->nullable();
            $table->string('details')->nullable();
            $table->text('description')->nullable();
            $table->integer('cost');
            $table->integer('role');
            $table->text('file_name')->nullable();
            $table->text('file_path')->nullable();
            $table->text('file_fullurl')->nullable();
            $table->text('file_orig_name')->nullable();
            $table->timestamps();
        });

        Schema::create('job_product', function (Blueprint $table) {
            $table->integer('job_id')->unsigned();
            $table->integer('product_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('job_product');
        Schema::drop('products');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('phone', 255)->nullable();
            $table->text('address')->nullable();
            $table->timestamps();
        });

        Schema::create('offices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->integer('client_id')->unsigned();
            $table->string('phone', 255)->nullable();
            $table->string('address', 255)->nullable();
            $table->timestamps();
        });

        Schema::create('office_user', function (Blueprint $table) {
            $table->integer('office_id')->unsigned();
            $table->integer('user_id')->unsigned();
        });

        Schema::create('client_user', function (Blueprint $table) {
            $table->integer('client_id')->unsigned();
            $table->integer('user_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('client_user');
        Schema::drop('office_user');
        Schema::drop('offices');
        Schema::drop('clients');
    }
}

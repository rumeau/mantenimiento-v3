<?php

use Illuminate\Database\Seeder;

class AddRoles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** SUPERADMIN */
        \HttpOz\Roles\Models\Role::create([
            'name' => 'Super Administrador',
            'slug' => 'superadmin',
            'description' => 'Control total del sistema',
            'group' => 'admins',
        ]);

        /** ADMIN */
        \HttpOz\Roles\Models\Role::create([
            'name' => 'Administrador',
            'slug' => 'admin',
            'description' => 'Administrador del sistema',
            'group' => 'admins',
        ]);

        /** CLIENTE */
        \HttpOz\Roles\Models\Role::create([
            'name' => 'Cliente',
            'slug' => 'client',
            'description' => 'Establece urgencia de ordenes de trabajo',
        ]);

        /** EMPRESA */
        \HttpOz\Roles\Models\Role::create([
            'name' => 'Empresa',
            'slug' => 'company',
            'description' => 'Ingresa, ejecuta y supervisa ordenes de trabajo',
        ]);

    }
}

<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $coll = \App\Models\Role::where('group', '!=', 'admins')->get();
        factory(\App\Models\User::class, 20)->create()->each(function ($u) use($coll) {

            /**
             * @var \Illuminate\Database\Eloquent\Collection $coll
             */
            $roles = $coll->random(1);
            $u->roles()->save($roles->first());
            $u->save();
        });
    }
}

(function () {

    var laroute = (function () {

        var routes = {

            absolute: false,
            rootUrl: 'http://mantv3.dev',
            routes : [{"host":null,"methods":["GET","HEAD"],"uri":"_debugbar\/open","name":"debugbar.openhandler","action":"Barryvdh\Debugbar\Controllers\OpenHandlerController@handle"},{"host":null,"methods":["GET","HEAD"],"uri":"_debugbar\/clockwork\/{id}","name":"debugbar.clockwork","action":"Barryvdh\Debugbar\Controllers\OpenHandlerController@clockwork"},{"host":null,"methods":["GET","HEAD"],"uri":"_debugbar\/assets\/stylesheets","name":"debugbar.assets.css","action":"Barryvdh\Debugbar\Controllers\AssetController@css"},{"host":null,"methods":["GET","HEAD"],"uri":"_debugbar\/assets\/javascript","name":"debugbar.assets.js","action":"Barryvdh\Debugbar\Controllers\AssetController@js"},{"host":null,"methods":["GET","HEAD"],"uri":"api\/user","name":null,"action":"Closure"},{"host":null,"methods":["GET","HEAD"],"uri":"healthcheck","name":null,"action":"Closure"},{"host":null,"methods":["GET","HEAD"],"uri":"login","name":"login","action":"App\Http\Controllers\Auth\LoginController@showLoginForm"},{"host":null,"methods":["POST"],"uri":"login","name":null,"action":"App\Http\Controllers\Auth\LoginController@login"},{"host":null,"methods":["POST"],"uri":"logout","name":"logout","action":"App\Http\Controllers\Auth\LoginController@logout"},{"host":null,"methods":["GET","HEAD"],"uri":"register","name":"register","action":"App\Http\Controllers\Auth\RegisterController@showRegistrationForm"},{"host":null,"methods":["POST"],"uri":"register","name":null,"action":"App\Http\Controllers\Auth\RegisterController@register"},{"host":null,"methods":["GET","HEAD"],"uri":"password\/reset","name":"password.request","action":"App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm"},{"host":null,"methods":["POST"],"uri":"password\/email","name":"password.email","action":"App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail"},{"host":null,"methods":["GET","HEAD"],"uri":"password\/reset\/{token}","name":"password.reset","action":"App\Http\Controllers\Auth\ResetPasswordController@showResetForm"},{"host":null,"methods":["POST"],"uri":"password\/reset","name":null,"action":"App\Http\Controllers\Auth\ResetPasswordController@reset"},{"host":null,"methods":["GET","HEAD"],"uri":"\/","name":null,"action":"Closure"},{"host":null,"methods":["GET","HEAD"],"uri":"home","name":"home","action":"App\Http\Controllers\HomeController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"clients","name":"clients","action":"App\Http\Controllers\ClientController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"clients\/create","name":"clients.create","action":"App\Http\Controllers\ClientController@create"},{"host":null,"methods":["POST"],"uri":"clients","name":"clients.store","action":"App\Http\Controllers\ClientController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"clients\/edit\/{client}","name":"clients.edit","action":"App\Http\Controllers\ClientController@edit"},{"host":null,"methods":["POST"],"uri":"clients\/edit\/{client}","name":"clients.update","action":"App\Http\Controllers\ClientController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"clients\/delete\/{client}","name":"clients.delete","action":"App\Http\Controllers\ClientController@delete"},{"host":null,"methods":["POST"],"uri":"clients\/destroy","name":"clients.destroy","action":"App\Http\Controllers\ClientController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"clients\/restore\/{client}","name":"clients.restore","action":"App\Http\Controllers\ClientController@restore"},{"host":null,"methods":["GET","HEAD"],"uri":"offices","name":"offices","action":"App\Http\Controllers\OfficeController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"offices\/create","name":"offices.create","action":"App\Http\Controllers\OfficeController@create"},{"host":null,"methods":["POST"],"uri":"offices","name":"offices.store","action":"App\Http\Controllers\OfficeController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"offices\/edit\/{office}","name":"offices.edit","action":"App\Http\Controllers\OfficeController@edit"},{"host":null,"methods":["POST"],"uri":"offices\/edit\/{office}","name":"offices.update","action":"App\Http\Controllers\OfficeController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"offices\/delete\/{office}","name":"offices.delete","action":"App\Http\Controllers\OfficeController@delete"},{"host":null,"methods":["POST"],"uri":"offices\/destroy","name":"offices.destroy","action":"App\Http\Controllers\OfficeController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"offices\/restore\/{office}","name":"offices.restore","action":"App\Http\Controllers\OfficeController@restore"},{"host":null,"methods":["GET","HEAD"],"uri":"offices\/{client}\/users","name":"offices.client.users","action":"App\Http\Controllers\OfficeController@users"},{"host":null,"methods":["GET","HEAD"],"uri":"users","name":"users","action":"App\Http\Controllers\UserController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"users\/create","name":"users.create","action":"App\Http\Controllers\UserController@create"},{"host":null,"methods":["POST"],"uri":"users","name":"users.store","action":"App\Http\Controllers\UserController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"users\/edit\/{user}","name":"users.edit","action":"App\Http\Controllers\UserController@edit"},{"host":null,"methods":["POST"],"uri":"users\/edit\/{user}","name":"users.update","action":"App\Http\Controllers\UserController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"users\/delete\/{user}","name":"users.delete","action":"App\Http\Controllers\UserController@delete"},{"host":null,"methods":["POST"],"uri":"users\/destroy","name":"users.destroy","action":"App\Http\Controllers\UserController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"users\/restore\/{user}","name":"users.restore","action":"App\Http\Controllers\UserController@restore"},{"host":null,"methods":["GET","HEAD"],"uri":"settings","name":"settings","action":"App\Http\Controllers\SettingsController@index"},{"host":null,"methods":["POST"],"uri":"settings","name":"settings.save","action":"App\Http\Controllers\SettingsController@save"},{"host":null,"methods":["GET","HEAD"],"uri":"profile","name":"profile","action":"App\Http\Controllers\ProfileController@index"},{"host":null,"methods":["POST"],"uri":"profile","name":"profile.save","action":"App\Http\Controllers\ProfileController@save"},{"host":null,"methods":["GET","HEAD"],"uri":"jobs","name":"jobs","action":"App\Http\Controllers\JobController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"jobs\/create","name":"jobs.create","action":"App\Http\Controllers\JobController@create"},{"host":null,"methods":["POST"],"uri":"jobs","name":"jobs.store","action":"App\Http\Controllers\JobController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"jobs\/edit\/{job}","name":"jobs.edit","action":"App\Http\Controllers\JobController@edit"},{"host":null,"methods":["POST"],"uri":"jobs\/edit\/{job}","name":"jobs.update","action":"App\Http\Controllers\JobController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"jobs\/delete\/{job}","name":"jobs.delete","action":"App\Http\Controllers\JobController@delete"},{"host":null,"methods":["POST"],"uri":"jobs\/destroy","name":"jobs.destroy","action":"App\Http\Controllers\JobController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"jobs\/restore\/{job}","name":"jobs.restore","action":"App\Http\Controllers\JobController@restore"},{"host":null,"methods":["GET","HEAD"],"uri":"jobs\/costs\/products","name":"jobs.costs.products","action":"App\Http\Controllers\JobController@costProducts"},{"host":null,"methods":["GET","HEAD"],"uri":"jobs\/view\/{job}","name":"jobs.view","action":"App\Http\Controllers\JobController@view"},{"host":null,"methods":["POST"],"uri":"jobs\/edit-priority\/{job}","name":"jobs.update.priority","action":"App\Http\Controllers\JobController@updatePriority"},{"host":null,"methods":["POST"],"uri":"jobs\/complete\/{job}","name":"jobs.complete","action":"App\Http\Controllers\JobController@complete"},{"host":null,"methods":["POST"],"uri":"jobs\/rate\/{job}","name":"jobs.rate","action":"App\Http\Controllers\JobController@rate"},{"host":null,"methods":["GET","HEAD"],"uri":"select-client","name":"select.client","action":"App\Http\Controllers\JobController@client"},{"host":null,"methods":["POST"],"uri":"select-client\/post","name":"select.client.post","action":"App\Http\Controllers\JobController@postClient"}],
            prefix: '',

            route : function (name, parameters, route) {
                route = route || this.getByName(name);

                if ( ! route ) {
                    return undefined;
                }

                return this.toRoute(route, parameters);
            },

            url: function (url, parameters) {
                parameters = parameters || [];

                var uri = url + '/' + parameters.join('/');

                return this.getCorrectUrl(uri);
            },

            toRoute : function (route, parameters) {
                var uri = this.replaceNamedParameters(route.uri, parameters);
                var qs  = this.getRouteQueryString(parameters);

                if (this.absolute && this.isOtherHost(route)){
                    return "//" + route.host + "/" + uri + qs;
                }

                return this.getCorrectUrl(uri + qs);
            },

            isOtherHost: function (route){
                return route.host && route.host != window.location.hostname;
            },

            replaceNamedParameters : function (uri, parameters) {
                uri = uri.replace(/\{(.*?)\??\}/g, function(match, key) {
                    if (parameters.hasOwnProperty(key)) {
                        var value = parameters[key];
                        delete parameters[key];
                        return value;
                    } else {
                        return match;
                    }
                });

                // Strip out any optional parameters that were not given
                uri = uri.replace(/\/\{.*?\?\}/g, '');

                return uri;
            },

            getRouteQueryString : function (parameters) {
                var qs = [];
                for (var key in parameters) {
                    if (parameters.hasOwnProperty(key)) {
                        qs.push(key + '=' + parameters[key]);
                    }
                }

                if (qs.length < 1) {
                    return '';
                }

                return '?' + qs.join('&');
            },

            getByName : function (name) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].name === name) {
                        return this.routes[key];
                    }
                }
            },

            getByAction : function(action) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].action === action) {
                        return this.routes[key];
                    }
                }
            },

            getCorrectUrl: function (uri) {
                var url = this.prefix + '/' + uri.replace(/^\/?/, '');

                if ( ! this.absolute) {
                    return url;
                }

                return this.rootUrl.replace('/\/?$/', '') + url;
            }
        };

        var getLinkAttributes = function(attributes) {
            if ( ! attributes) {
                return '';
            }

            var attrs = [];
            for (var key in attributes) {
                if (attributes.hasOwnProperty(key)) {
                    attrs.push(key + '="' + attributes[key] + '"');
                }
            }

            return attrs.join(' ');
        };

        var getHtmlLink = function (url, title, attributes) {
            title      = title || url;
            attributes = getLinkAttributes(attributes);

            return '<a href="' + url + '" ' + attributes + '>' + title + '</a>';
        };

        return {
            // Generate a url for a given controller action.
            // laroute.action('HomeController@getIndex', [params = {}])
            action : function (name, parameters) {
                parameters = parameters || {};

                return routes.route(name, parameters, routes.getByAction(name));
            },

            // Generate a url for a given named route.
            // laroute.route('routeName', [params = {}])
            route : function (route, parameters) {
                parameters = parameters || {};

                return routes.route(route, parameters);
            },

            // Generate a fully qualified URL to the given path.
            // laroute.route('url', [params = {}])
            url : function (route, parameters) {
                parameters = parameters || {};

                return routes.url(route, parameters);
            },

            // Generate a html link to the given url.
            // laroute.link_to('foo/bar', [title = url], [attributes = {}])
            link_to : function (url, title, attributes) {
                url = this.url(url);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given route.
            // laroute.link_to_route('route.name', [title=url], [parameters = {}], [attributes = {}])
            link_to_route : function (route, title, parameters, attributes) {
                var url = this.route(route, parameters);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given controller action.
            // laroute.link_to_action('HomeController@getIndex', [title=url], [parameters = {}], [attributes = {}])
            link_to_action : function(action, title, parameters, attributes) {
                var url = this.action(action, parameters);

                return getHtmlLink(url, title, attributes);
            }

        };

    }).call(this);

    /**
     * Expose the class either via AMD, CommonJS or the global object
     */
    if (typeof define === 'function' && define.amd) {
        define(function () {
            return laroute;
        });
    }
    else if (typeof module === 'object' && module.exports){
        module.exports = laroute;
    }
    else {
        window.laroute = laroute;
    }

}).call(this);


<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $me
     * @param $ability
     * @return bool
     */
    public function before(User $me, $ability)
    {
        if ($me->inGroup('admins')) {
            return true;
        }
    }

    /**
     * @param User $me
     * @return bool
     */
    public function list(User $me)
    {
        return false;
    }

    /**
     * Determine whether the user can view the user.
     *
     * @param  \App\Models\User  $me
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function view(User $me, User $user)
    {
        //
    }

    /**
     * Determine whether the user can create users.
     *
     * @param  \App\Models\User  $me
     * @return mixed
     */
    public function create(User $me)
    {
        return false;
    }

    public function store(User $me)
    {
        return false;
    }

    public function edit(User $me)
    {
        return false;
    }

    /**
     * Determine whether the user can update the user.
     *
     * @param  \App\Models\User  $me
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function update(User $me, User $user)
    {
        //
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function delete(User $me, User $user)
    {
        return $me->inGroup('admins');
    }
}

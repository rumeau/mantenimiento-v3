<?php

namespace App\Policies;

use App\Models\Job;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Auth\Access\HandlesAuthorization;

class JobPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($ability === 'update-job-priority') {
            return;
        }

        if ($user->inGroup('admins')) {
            return true;
        }
    }

    /**
     * @param User $user
     * @return bool
     */
    public function list(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the client.
     *
     * @param User $user
     * @param Job $job
     * @return mixed
     */
    public function view(User $user, Job $job)
    {
        if ($user->isRole('company')) {
            return true;
        }

        if ($user->isRole('client')) {
            if ((int) $job->status === Job::STATUS_PENDING) {
                return false;
            }

            return $user->offices->contains('id', $job->office_id);
        }

        return false;
    }

    /**
     * Determine whether the user can create clients.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->inGroup('company');
    }

    /**
     * Determine whether the user can update the client.
     *
     * @param  \App\Models\User $user
     * @param Job $job
     * @return mixed
     */
    public function update(User $user, Job $job)
    {
        if ($job->status === Job::STATUS_PENDING) {
            if ($user->isRole('client')) {
                return false;
            }
        }

        return $this->view($user, $job);
    }

    /**
     * Determine whether the user can delete the client.
     *
     * @param  \App\Models\User $user
     * @param Job $job
     */
    public function delete(User $user, Job $job)
    {

    }

    public function updateJobPriority(User $user, Job $job)
    {
        if (!$user->isRole('client')) {
            return false;
        }
        if ($job->priority > 1) {
            return false;
        }

        if (!$job->inprogress_at instanceof Carbon || Carbon::now()->greaterThan($job->inprogress_at->addMinutes(configger()->get('priority_tta', 120)))) {
            return false;
        }

        return true;
    }

    /**
     * @param User $user
     * @param Job $job
     * @return bool
     */
    public function complete(User $user, Job $job)
    {
        if ((int) $job->status > Job::STATUS_INPROGRESS) {
            return false;
        }

        if ($job->author->id === $user->id) {
            return true;
        }

        return false;
    }

    public function rate(User $user, Job $job)
    {
        if ((int) $job->status > Job::STATUS_COMPLETED) {
            return false;
        }

        if ($job->office->users->contains($user->id)) {
            return true;
        }

        return false;
    }
}

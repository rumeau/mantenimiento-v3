<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Office;
use Illuminate\Auth\Access\HandlesAuthorization;

class OfficePolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        if ($user->inGroup('admins')) {
            return true;
        }
    }

    public function list(User $user)
    {
        if ($user->isRole('company')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can view the office.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Office  $office
     * @return mixed
     */
    public function view(User $user, Office $office)
    {
        //
    }

    /**
     * Determine whether the user can create offices.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->inGroup('admins');
    }

    /**
     * Determine whether the user can update the office.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Office  $office
     * @return mixed
     */
    public function update(User $user, Office $office)
    {
        return $user->inGroup('admins');
    }

    /**
     * Determine whether the user can delete the office.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Office  $office
     * @return mixed
     */
    public function delete(User $user, Office $office)
    {
        return $user->inGroup('admins');
    }
}

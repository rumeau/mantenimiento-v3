<?php
/**
 * Created by PhpStorm.
 * User: Jean Rumeau
 * Date: 05/04/2017
 * Time: 12:27
 */

if (!function_exists('user')) {
    /**
     * @return null
     */
    function user() {
        if (\Auth::check()) {
            return \Auth::user();
        }

        return null;
    }
}

if (!function_exists('clients_offices_array_select')) {
    /**
     * @return array
     */
    function clients_offices_array_select()
    {
        $clients = \App\Models\Client::with('offices')->get();
        $array = [];
        foreach ($clients as $client) {
            if (!$client->offices->count()) {
                continue;
            }

            foreach ($client->offices as $office) {
                $array[$client->name][$client->id . '_' . $office->id] = $office->name;
            }
        }

        return $array;
    }
}

if (!function_exists('clients_array_select')) {
    /**
     * @return \Illuminate\Support\Collection
     */
    function clients_array_select()
    {
        return \App\Models\Client::all()->pluck('name', 'id');
    }
}

if (!function_exists('configger')) {
    /**
     * @return \D3lph1\LaravelConfigger\Configger
     */
    function configger() {
        return app(\D3lph1\LaravelConfigger\Configger::class);
    }
}

if (!function_exists('password_strength_combo')) {
    /**
     * @return array
     */
    function password_strength_combo() {
        return [
            'kids' => 'Para niños (no recomendada)',
            'basic' => 'Básica (letras y números)',
            'complex' => 'Compleja (Letras mayúsculas y minúsculas con números)',
            'safe' => 'Segura (Mayúsculas, minúsculas, números y símbolos',
        ];
    }
}

if (!function_exists('html_label')) {
    /**
     * @param $text
     * @param string $class
     * @return string
     */
    function html_label($text, $class = 'default') {
        return '<span class="tag tag-' . $class . '">' . $text . '</span>';
    }
}

if (!function_exists('label_priority')) {
    /**
     * @param $priority
     * @return string
     */
    function label_priority($priority) {
        switch ($priority) {
            default :
            case \App\Models\Job::PRIORITY_NORMAL :
                $html = html_label('Normal', 'default');
                break;
        }

        return $html;
    }
}

if (!function_exists('label_status')) {
    /**
     * @param $status
     * @return string
     */
    function label_status($status) {
        switch ($status) {
            default :
            case \App\Models\Job::STATUS_INPROGRESS :
                $html = html_label('EN EJECUCION', 'default');
                break;

            case \App\Models\Job::STATUS_PENDING :
                $html = html_label('PENDIENTE DE COTIZACION', 'warning');
                break;

            case \App\Models\Job::STATUS_COMPLETED :
                $html = html_label('EJECUTADA', 'success');
                break;

            case \App\Models\Job::STATUS_RECEIVED :
                $html = html_label('RECEPCIONADA', 'primary');
                break;

            case \App\Models\Job::STATUS_REJECTED :
                $html = html_label('RECHAZADA', 'danger');
                break;
        }

        return $html;
    }
}

if (!function_exists('user_clients_filter_items')) {
    /**
     * @return string
     */
    function user_clients_filter_items() {
        $user = \Auth::user();
        if ($user->group() == 'admins') {
            $filters = \App\Models\Client::all(['id', 'name'])->toArray();
        } else {
            $filters = $user->clients->only(['id', 'name'])->toArray();
        }

        array_unshift($filters, ['id' => '', 'name' => '-- Cliente --']);

        return json_encode($filters);
    }
}

if (!function_exists('roles_filter_items')) {
    /**
     * @return string
     */
    function roles_filter_items() {
        $roles = \App\Models\Role::all(['id', 'name'])->toArray();
        array_unshift($roles, ['id' => '', 'name' => '-- Tipo de Usuario --']);
        return json_encode($roles);
    }
}

if (!function_exists('user_status_filter_items')) {
    function user_status_filter_items() {
        return json_encode([
            [
                'id' => '-1', 'name' => 'Todos',
            ], [
                'id' => '0', 'name' => 'Inactivos',
            ], [
                'id' => '1', 'name' => 'Activos',
            ],
        ]);
    }
}

if (!function_exists('user_offices_filter_items')) {
    function user_offices_filter_items() {
        $user = \Auth::user();
        if ($user->group() == 'default') {
            $offices = $user->offices()->where('client_id', $user->client->id)->get(['name', 'id'])->toArray();
        } else {
            $offices = \App\Models\Office::where('client_id', $user->client->id)->get(['name', 'id'])->toArray();
        }

        array_unshift($offices, ['id' => '', 'name' => '-- Sede --']);

        return json_encode($offices);
    }
}

if (!function_exists('job_status_filter_items')) {
    function job_status_filter_items() {
        $filters = [];
        $filters[] = ['id' => '', 'name' => '-- Estado --'];
        $filters[] = ['id' => \App\Models\Job::STATUS_INPROGRESS, 'name' => 'EN EJECUCION'];

        if (!Auth::user()->isRole('client')) {
            $filters[] = ['id' => \App\Models\Job::STATUS_PENDING, 'name' => 'PENDIENTE DE COTIZACION'];
        }

        $filters[] = ['id' => \App\Models\Job::STATUS_COMPLETED, 'name' => 'EJECUTADA'];
        $filters[] = ['id' => \App\Models\Job::STATUS_RECEIVED, 'name' => 'APROBADA'];
        $filters[] = ['id' => \App\Models\Job::STATUS_REJECTED, 'name' => 'RECHAZADA'];

        return json_encode($filters);
    }
}

if (!function_exists('priorities_collection')) {
    function priorities_collection() {
        return collect([
            ['id' => \App\Models\Job::PRIORITY_NORMAL, 'name' => 'Normal'],
            ['id' => \App\Models\Job::PRIORITY_URGENT, 'name' => 'Urgente'],
        ]);
    }
}
if (!function_exists('priority_filter_items')) {
    function priority_filter_items() {
        $priorities = priorities_collection()->toArray();
        array_unshift($priorities, ['id' => '', 'name' => '-- Prioridad --']);
        return json_encode($priorities);
    }
}

if (!function_exists('get_job_types')) {
    function get_job_types() {
        $jobTypes = configger()->get('job_types', '');
        $options = explode(',', $jobTypes);
        $optionsArray = array_map('trim', $options);

        return $optionsArray;
    }
}

if (!function_exists('costs_errors_parser')) {
    function costs_errors_parser($errors) {
        $err = $errors->get('costs.*');
        if (count($err) == 0) {
            return json_encode([]);
        }

        $return = [];
        foreach ($err as $e => $msg) {
            $parts = explode('.', $e);
            $return[$parts[2]] = true;
        }

        return json_encode($return);
    }
}
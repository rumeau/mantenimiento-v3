<?php
/**
 * Created by PhpStorm.
 * User: Jean Rumeau
 * Date: 07/04/2017
 * Time: 0:02
 */

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class UserCreated
 * @package App\Notifications
 */
class UpdatePassword extends Notification
{
    /**
     * The password.
     *
     * @var string
     */
    public $password;

    /**
     * Create a notification instance.
     *
     * @param  string $password
     */
    public function __construct($password)
    {
        $this->password = $password;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('Su contraseña ha sido modificada en ' . config('app.name'))
            ->action('Iniciar sesión', url('login'))
            ->line('Puede ingresar usando su dirección de correo, con la clave temporal que hemos generado para ud.:')
            ->line($this->password);
    }
}

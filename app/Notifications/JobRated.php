<?php

namespace App\Notifications;

use App\Models\Job;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class JobRated extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed|Job  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $text = 'modificada';
        if ($notifiable->status === Job::STATUS_RECEIVED) {
            $text = 'Aprobada';
        } elseif ($notifiable->status === Job::STATUS_REJECTED) {
            $text = 'Rechazada';
        }

        return (new MailMessage)
            ->subject('OT #' . $notifiable->id)
            ->greeting('Estimado usuario.')
            ->line('La OT #' . $notifiable->id . ' fue ' . $text . '.')
            ->line('Cliente: "' . $notifiable->client->name . '"')
            ->line('Sede: "' . $notifiable->office->name)
            ->action('Ver OT', route('jobs.view', ['job' => $notifiable->id]))
            ->line('PD: No responder este correo.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

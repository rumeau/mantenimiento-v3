<?php
/**
 * Created by PhpStorm.
 * User: Jean Rumeau
 * Date: 06/04/2017
 * Time: 9:59
 */

namespace App\Notifications;


use Illuminate\Auth\Notifications\ResetPassword as CoreResetPassword;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPassword extends CoreResetPassword
{
    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line(trans('passwords.notification.line_1'))
            ->action(trans('passwords.notification.action'), route('password.reset', $this->token))
            ->line(trans('passwords.notification.line_2'));
    }
}
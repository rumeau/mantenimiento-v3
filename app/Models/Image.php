<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Image
 *
 * @property int $id
 * @property string $name
 * @property string $path
 * @property string $fullurl
 * @property string $orig_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $job_id
 * @property-read \App\Models\Job $job
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Image whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Image whereFullurl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Image whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Image whereJobId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Image whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Image whereOrigName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Image wherePath($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Image whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Image extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'job_id',
        'name',
        'path',
        'fullurl',
        'orig_name',
    ];

    public function job()
    {
        return $this->belongsTo('App\Models\Job');
    }
}

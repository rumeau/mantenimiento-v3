<?php

namespace App\Models;

use App\Scopes\OrderByNameScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Client
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Office[] $offices
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $address
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereUpdatedAt($value)
 * @property-read mixed $trashed
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 */
class Client extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'phone',
        'address',
    ];

    protected $appends = [
        'trashed',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new OrderByNameScope);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function offices()
    {
        return $this->hasMany('App\Models\Office');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'client_user');
    }

    public function getTrashedAttribute()
    {
        return $this->trashed();
    }

    public function getFormValue($key)
    {
        if ($key == 'users') {
            return $this->users()->whereHas('roles', function ($q) {
                $q->where('roles.slug', 'client');
            })->pluck('id')->toArray();
        }

        return $this->getAttributeValue($key);
    }
}

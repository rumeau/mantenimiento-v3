<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Office
 *
 * @property-read \App\Models\Client $client
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property int $client_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Office whereClientId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Office whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Office whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Office whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Office whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Office whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 */
class Office extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'client_id',
    ];

    protected $dates = [
        'deleted_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'office_user');
    }

    public function getFormValue($key)
    {
        if ($key == 'users') {
            return $this->users->pluck('id')->toArray();
        }

        return $this->getAttributeValue($key);
    }
}

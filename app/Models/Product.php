<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Product
 *
 * @property int $id
 * @property string $type
 * @property string $product
 * @property string $details
 * @property string $description
 * @property int $cost
 * @property int $role
 * @property string $file_name
 * @property string $file_path
 * @property string $file_fullurl
 * @property string $file_orig_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereCost($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereDetails($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereFileFullurl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereFileName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereFileOrigName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereFilePath($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereProduct($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereRole($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Product extends Model
{
    const PRODUCT_TYPE_MATERIAL = 1;
    const PRODUCT_TYPE_RRHH = 2;
    const PRODUCT_TYPE_QUOTE = 3;
    const PRODUCT_TYPE_OTHER = 4;
    const PRODUCT_TYPE_EXTRA = 5;

    protected $fillable = [
        'type',
        'product',
        'details',
        'description',
        'cost',
        'role',
        'file_name',
        'file_path',
        'file_fullurl',
        'file_orig_name',
    ];
}

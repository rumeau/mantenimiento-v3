<?php
/**
 * Created by PhpStorm.
 * User: Jean Rumeau
 * Date: 06/04/2017
 * Time: 22:51
 */

namespace App\Models;

use App\Traits\Sluggable;
use Illuminate\Database\Eloquent\Model;

use App\Traits\RoleHasRelations;
use App\Contracts\RoleHasRelations as RoleHasRelationsContract;
use HttpOz\Roles\Models\Role as CoreRole;

/**
 * App\Models\Role
 *
 * @property-read mixed $group
 * @property-read mixed $slug
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Role whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Role whereGroup($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Role whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Role whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Role whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Role whereUpdatedAt($value)
 */
class Role extends Model implements RoleHasRelationsContract
{
    use Sluggable, RoleHasRelations;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'description', 'group'];

    /**
     * Create a new model instance.
     *
     * @param array $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        if ($connection = config('roles.connection')) {
            $this->connection = $connection;
        }
    }

}
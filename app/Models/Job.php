<?php

namespace App\Models;

use App\Notifications\JobCompleted;
use App\Notifications\JobPriorityUpdated;
use App\Notifications\JobRated;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use App\Notifications\JobCreated;
use Illuminate\Notifications\Notifiable;

/**
 * App\Models\Job
 *
 * @property-read \App\Models\User $author
 * @property-read \App\Models\Client $client
 * @property-read \App\Models\Office $office
 * @property-read \App\Models\User $responsible
 * @property-read Collection|\App\Models\Product[] $products
 * @mixin \Eloquent
 * @property int $id
 * @property int $status
 * @property int $priority
 * @property string $description
 * @property int $type
 * @property int $client_id
 * @property int $office_id
 * @property int $author_id
 * @property int $responsible_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $starting_at
 * @property \Carbon\Carbon $started_at
 * @property \Carbon\Carbon $ending_at
 * @property \Carbon\Carbon $ended_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Job whereAuthorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Job whereClientId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Job whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Job whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Job whereEndedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Job whereEndingAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Job whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Job whereOfficeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Job wherePriority($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Job whereResponsibleId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Job whereStartedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Job whereStartingAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Job whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Job whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Job whereUpdatedAt($value)
 * @property int $viewed
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Job whereViewed($value)
 */
class Job extends Model
{
    use Notifiable;

    const PRIORITY_NORMAL = 1;
    const PRIORITY_URGENT = 2;

    const STATUS_INPROGRESS = 1;
    const STATUS_PENDING = 2;
    const STATUS_COMPLETED = 3;
    const STATUS_RECEIVED = 4;
    const STATUS_REJECTED = 5;

    protected $dates = [
        'starting_at',
        'started_at',
        'ending_at',
        'ended_at',
        'inprogress_at',
    ];

    protected $recipients = [];

    protected $fillable = [
        'status',
        'priority',
        'description',
        'type',
        'client_id',
        'office_id',
        'author_id',
        'responsible_id',
        'starting_at',
        'started_at',
        'ending_at',
        'ended_at',
        'inprogress_at',
        'viewed',
        'job_type',
        'title',
    ];

    protected $appends = [
        'amount',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function office()
    {
        return $this->belongsTo('App\Models\Office');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('App\Models\User', 'author_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function responsible()
    {
        return $this->belongsTo('App\Models\User', 'responsible_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'job_product')->withPivot('cost as cost', 'amount as amount', 'total');
    }

    public function images()
    {
        return $this->hasMany('App\Models\Image');
    }

    public function getFormValue($attribute)
    {
        switch ($attribute) {
            case 'starting_at' :
                $result = $this->starting_at->format('d/m/Y');
                break;

            case 'ending_at':
                $result = $this->ending_at->format('d/m/Y');
                break;

            default:
                $result = $this->getAttribute($attribute);
                break;
        }

        return $result;
    }

    public function getAmountAttribute()
    {
        if (isset($this->attributes['amount'])) {
            return $this->attributes['amount'];
        }

        if (isset($this->pivot->amount)) {
            return $this->pivot->amount;
        }

        return 0;
    }

    public function sendCreatedToClientsNotification()
    {
        $recipients = [];
        $users = $this->office->users;
        foreach ($users as $user) {
            $recipients[] = $user->email;
        }
        $this->setRecipients($recipients);

        $this->notify(new JobCreated);
    }

    public function sendPriorityUpdateToCompanyNotification()
    {
        $this->setRecipients([
            $this->author->email
        ]);

        $this->notify(new JobPriorityUpdated);
    }

    public function sendCompletedToClientNotification()
    {
        $recipients = [];
        $users = $this->office->users;
        foreach ($users as $user) {
            $recipients[] = $user->email;
        }
        $this->setRecipients($recipients);

        $this->notify(new JobCompleted);
    }

    public function sendRatedToCompanyNotification()
    {
        $this->setRecipients([
            $this->author->email
        ]);

        $this->notify(new JobRated);
    }

    protected function setRecipients($recipients)
    {
        $this->recipients = $recipients;
    }

    public function routeNotificationForMail()
    {
        return $this->recipients;
    }
}

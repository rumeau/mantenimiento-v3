<?php

namespace App\Models;

use App\Notifications\UserCreated;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\HasRole;
use Illuminate\Support\Facades\Session;
use App\Contracts\HasRole as HasRoleContract;
use App\Notifications\ResetPassword as ResetPasswordNotification;
use App\Notifications\UpdatePassword as UpdatePasswordNotification;

/**
 * App\Models\User
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Client[] $clients
 * @property-read string $avatar
 * @property mixed $client
 * @property-read mixed $role
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Office[] $offices
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $roles
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereUpdatedAt($value)
 * @property-read string $role_name
 * @property-read bool $status
 * @property-read bool $trashed
 */
class User extends Authenticatable implements HasRoleContract
{
    use SoftDeletes, Notifiable, HasRole;

    protected $appends = [
        'role_name',
        'trashed',
        'status',
        'client',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'update_password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'deleted_at',
    ];

    /**
     * @var array
     */
    protected $sessionLoad = [];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function updatePasswordNotification($password)
    {
        $this->notify(new UpdatePasswordNotification($password));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function clients()
    {
        return $this->belongsToMany('App\Models\Client', 'client_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function offices()
    {
        return $this->belongsToMany('App\Models\Office', 'office_user');
    }

    /**
     * @return mixed|null
     */
    public function getRoleAttribute()
    {
        $roles = $this->roles()->get();
        if ($roles->count() > 0) {
            return $roles->first();
        }

        return null;
    }

    /**
     * @return bool
     */
    public function getTrashedAttribute()
    {
        return $this->trashed();
    }

    /**
     * @return bool
     */
    public function getStatusAttribute()
    {
        return !$this->trashed();
    }

    /**
     * @return string
     */
    public function getRoleNameAttribute()
    {
        $role = $this->roles()->first();
        if ($role != null) {
            return $role->name;
        }

        return '';
    }

    /**
     * @return mixed
     */
    public function getClientAttribute()
    {
        if (!isset($this->sessionLoad['client'])) {
            if ($this->clients()->count() === 1) {
                $client = $this->clients()->first();
                $this->sessionLoad['client'] = $client;
            } else {
                $clientId = Session::get('currentClient');
                $client = Client::find($clientId);
                if ($client instanceof Client) {
                    $this->sessionLoad['client'] = $client;
                } else {
                    $this->sessionLoad['client'] = false;
                }
            }
        }

        return $this->sessionLoad['client'];
    }

    /**
     * @param $value
     */
    public function setClientAttribute($value)
    {
        Session::put('currentClient', $value->id);
        $this->sessionLoad['client'] = $value;
    }

    public function sendUserCreatedNotification($password)
    {
        $this->notify(new UserCreated($password));
    }


    public function getFormValue($attribute)
    {
        switch ($attribute) {
            case 'roles' :
                $role = $this->getRoleAttribute();
                $result = is_null($role) ? null : $role->slug;
                break;

            case 'offices':
                $clients = $this->clients()->get();
                $offices = $this->offices()->get();
                $officesArray = [];
                foreach ($clients as $client) {
                    $officesArray[$client->id] = $client->id;
                }
                foreach ($offices as $office) {
                    if (in_array($office->client_id, $officesArray)) {
                        unset($officesArray[$office->client_id]);
                    }
                    $officesArray[$office->client_id . '_' . $office->id] = $office->client_id . '_' . $office->id;
                }

                $result = $officesArray;
                break;
            default:
                $result = $this->getAttribute($attribute);
                break;
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getAvatarAttribute()
    {
        $default = 'http://lorempixel.com/80/80/animals';
        $size = 80;
        $hash = md5(strtolower(trim($this->attributes['email'])));

        return "http://www.gravatar.com/avatar/$hash?d=$default&s=$size";
    }
}

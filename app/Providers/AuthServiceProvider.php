<?php

namespace App\Providers;

use App\Models\Client;
use App\Models\Office;
use App\Models\User;
use App\Models\Job;
use App\Policies\ClientPolicy;
use App\Policies\JobPolicy;
use App\Policies\OfficePolicy;
use App\Policies\UserPolicy;
use Carbon\Carbon;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Client::class => ClientPolicy::class,
        Office::class => OfficePolicy::class,
        User::class => UserPolicy::class,
        Job::class => JobPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $this->registerBladeExtensions();

        Gate::define('list-client-users', function (User $user) {
            return $user->group() == 'admins';
        });

        Gate::define('update-settings', function (User $user) {
            return $user->group() == 'admins';
        });

        Gate::define('update-profile', function (User $user) {
            return $user->id == \Auth::user()->id;
        });


        Gate::define('create-job', function (User $user) {
            return $user->group() == 'company' || $user->group() == 'admins';
        });

        Gate::define('view-job', function (User $user, Job $job) {
            if ($user->inGroup('admins')) {
                return true;
            }

            if ($user->clients->contains($job->client)) {
                if ($job->office instanceof Office) {
                    if ($user->offices->contains($job->office)) {
                        return true;
                    }
                } else {
                    return true;
                }
            }

            return false;
        });

        try {
            config(['session.lifetime' => configger()->get('session_ttl', 120)]);
        } catch (\Exception $e) {
            // Configger tables not available yet
        }
    }

    public function registerBladeExtensions()
    {

        Blade::directive('role', function ($expression) {
            return "<?php if (Auth::check() && Auth::user()->isRole($expression)): ?>";
        });

        Blade::directive('endrole', function () {
            return "<?php endif; ?>";
        });

        Blade::directive('group', function ($expression) {
            return "<?php if (Auth::check() && Auth::user()->group() == $expression): ?>";
        });
        Blade::directive('endgroup', function () {
            return "<?php endif; ?>";
        });
    }
}

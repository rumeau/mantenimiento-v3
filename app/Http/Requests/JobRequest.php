<?php

namespace App\Http\Requests;

use App\Models\Job;
use App\Models\Office;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class JobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (!$this->user() instanceof User) {
            return false;
        }

        return $this->user()->group() != 'default';
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /**
         * @var User $user
         */
        $user = $this->user();
        if ($user->group() == 'default') {
            $offices = $user->offices()->where('client_id', $user->client->id)->get();
        } else {
            $offices = Office::where('client_id', $user->client->id)->get();
        }

        $rules = [
            'priority' => 'sometimes',
            'title' => 'required',
            'description' => 'present',
            'images' => 'sometimes|array',
            'images.*' => 'sometimes|image',
            'starting_at' => 'required|date_format:d/m/Y|before_or_equal:ending_at',
            'ending_at' => 'required|date_format:d/m/Y|after_or_equal:starting_at',
            'costs' => 'sometimes|min:0',
            'costs.materials' => 'sometimes',
            'costs.materials.*.type' => 'required',
            'costs.materials.*.product' => 'required',
            'costs.materials.*.amount' => 'required',

            'costs.rrhh' => 'sometimes',
            'costs.rrhh.*.type' => 'required',
            'costs.rrhh.*.amount' => 'required',

            'costs.extra' => 'sometimes',
            'costs.extra.*.product' => 'required',
            'costs.extra.*.cost' => 'required',

            'costs.other' => 'sometimes',
            'costs.other.*.type' => 'required',
            'costs.other.*.product' => 'required',
            'costs.other.*.amount' => 'required',
            'costs.other.*.cost' => 'required',

            'costs.quote' => 'sometimes',
            'costs.quote.*.description' => 'required',
            'costs.quote.*.cost' => 'required',
        ];

        if (!$this->route('job', false)) {
            $rules['office'] = [
                'required',
                'in:' . $offices->implode('id', ','),
            ];

            $rules['status'] = [
                'required',
                'in:' . Job::STATUS_INPROGRESS . ',' . Job::STATUS_PENDING
            ];

            $rules['costs.quote.*.file'] = 'sometimes|file';
        }

        return $rules;
    }
}

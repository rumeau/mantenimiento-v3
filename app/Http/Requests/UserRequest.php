<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return \Auth::user()->inGroup('admins');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'roles' => ['required'],
        ];

        if (!Auth::user()->isRole('superadmin')) {
            $rules['roles'][] = 'not_in:superadmin';
        }

        $rules['roles'][] = 'exists:roles,slug';

        if (!($user = $this->route('user')) instanceof User) {
            $rules['email'] = 'required|email|unique:users';
            //$rules['password'] = 'required|min:4|confirmed';
        } else {
            if ($this->input('password', null) != '') {
                $rules['password'] = self::passwordRules();
            }
        }

        if ($this->input('roles') == 'client') {
            $rules['offices'] = [
                'sometimes',
                'array',
            ];
        }

        return $rules;
    }

    public static function passwordRules()
    {
        $passwordLength = configger()->get('password_length', 6);
        $passwordStrength = configger()->get('password_strength', 'basic');

        $rules = [
            'required',
            'min:' . $passwordLength,
            'confirmed'
        ];

        switch ($passwordStrength) {
            case 'basic':
                $rules[] = 'letters';
                $rules[] = 'numbers';
                break;
            case 'complex':
                $rules[] = 'letters';
                $rules[] = 'numbers';
                $rules[] = 'case_diff';
                break;
            case 'safe':
                $rules[] = 'letters';
                $rules[] = 'numbers';
                $rules[] = 'case_diff';
                $rules[] = 'symbols';
                break;
        }

        return $rules;
    }
}

<?php

namespace App\Http\Requests;

use App\Models\Office;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OfficeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = \Auth::user();
        if ($user->inGroup('admins')){
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        $office = $this->route('office');
        if (!$office instanceof Office) {
            $clientId = $this->input('client_id');
            $unique   = $unique = Rule::unique('offices')->where(function ($query) use ($clientId){
                $query->where('client_id', '=', $clientId);
            })->ignore($office instanceof Office ? $office->id : null);

            $rules['name'] = [
                'required',
                $unique
            ];
        } else {
            $rules['name'] = ['required'];
        }

        $rules['client_id'] = 'required|exists:clients,id';

        return $rules;
    }
}

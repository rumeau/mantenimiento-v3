<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckPasswordUpdate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && (int) $request->user()->update_password === 1) {
            flash()->info('Por favor actualice su clave');
            $user = $request->user();
            $user->update_password = 0;
            $user->save();
            return redirect()->route('profile');
        }

        return $next($request);
    }
}

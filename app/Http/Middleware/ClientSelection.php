<?php

namespace App\Http\Middleware;

use App\Models\Client;
use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class ClientSelection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        /**
         * @var User $user
         */
        $user = Auth::guard($guard)->user();
        if (!$user->client instanceof Client) {
            return redirect('/select-client');
        }

        return $next($request);
    }
}

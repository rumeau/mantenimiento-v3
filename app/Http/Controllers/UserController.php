<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\Role;
use App\Models\User;
use App\Scopes\OrderByNameScope;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * @param Request $request
     * @param Guard $auth
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|array
     */
    public function index(Request $request, Guard $auth)
    {
        $filters = $request->query('filter', []);
        $query = User::withoutGlobalScope(OrderByNameScope::class);
        $query->where('id', '!=', $auth->user()->id);

        if (isset($filters['status'])) {
            if ($filters['status'] == -1) {
                $query->withTrashed();
            } elseif ($filters['status'] == 0) {
                $query->onlyTrashed();
            }
        }

        if (isset($filters['role']) && $filters['role'] != '') {
            $query->whereHas('roles', function ($q) use ($filters) {
                $q->where('roles.id', $filters['role']);
            });
        }

        if (isset($filters['client_id']) && $filters['client_id'] != '') {
            $query->has('clients');
        }

        $users = $query->paginate($request->query('limit', 2));

        /**
         * @var LengthAwarePaginator $clients
         */
        if ($request->isXmlHttpRequest()) {
            return [
                'data' => $users->items(),
                'itemsCount' => $users->total(),
            ];
        }

        return view('users.index', compact('users'));
    }

    /**
     * @param Guard $auth
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Guard $auth)
    {
        $roles = $this->authorizedRoles($auth);

        return view('users.create', compact('roles'));
    }

    /**
     * @param $auth
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    protected function authorizedRoles($auth)
    {
        if ($auth->user()->isRole('superadmin')) {
            $roles = Role::all();
        } else {
            $roles = Role::where('slug', '!=', 'superadmin')->get();
        }

        return $roles;
    }

    /**
     * @param UserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserRequest $request)
    {
        $role = Role::where('slug', $request->input('roles'))->firstOrFail();
        // prepara clientes y sedes
        $clients = [];
        $offices = [];
        if ($role->group !== 'admins') {
            $this->prepareOffices($request->input('offices', []), $clients, $offices);
        }

        $password = str_random(8);
        $cryptPassword = bcrypt($password);
        try {
            $data = array_merge($request->only('name', 'email'), [
                'password' => $cryptPassword,
                'update_password' => 1
            ]);
            /**
             * @var User $user
             */
            $user = User::create($data);
            $user->syncRoles([$role->id]);
            $user->clients()->sync($clients);
            $user->offices()->sync($offices);
            $user->save();

            $user->sendUserCreatedNotification($password);
            flash()->success('La cuenta de usuario ha sido guardada');
        } catch (\Exception $e) {
            flash()->error('Se ha producido un error al guardar el usuario: ' . $e->getMessage());
            return redirect()->back()->withInput();
        }

        return redirect()->route('users');
    }

    /**
     * @param User $user
     * @param Guard $auth
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user, Guard $auth)
    {
        $roles = $this->authorizedRoles($auth);
        return view('users.edit', compact('user', 'roles'));
    }

    /**
     * @param UserRequest $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserRequest $request, User $user)
    {
        $role = Role::where('slug', $request->input('roles'))->firstOrFail();
        // prepara clientes y sedes
        $clients = [];
        $offices = [];
        if ($role->group !== 'admins') {
            $this->prepareOffices($request->input('offices', []), $clients, $offices);
        }

        $data = array_merge($request->only('name'));
        if (($password = $request->input('password', null)) != '') {
            $data['password'] = bcrypt($password);
        }

        try {
            $user->update($data);
            $user->syncRoles([$role->id]);
            $user->clients()->sync($clients);
            $user->offices()->sync($offices);
            $user->save();

            if (!is_null($password)) {
                $user->updatePasswordNotification($password);
            }

            flash()->success('La cuenta del usuario ha sido modicada');
        } catch (\Exception $e) {
            flash()->error('Se ha producido un error al guardar el usuario: ' . $e->getMessage());
            return redirect()->back()->withInput();
        }

        return redirect()->route('users');
    }

    /**
     * @param $requestOffices
     * @param $clients
     * @param $offices
     */
    protected function prepareOffices($requestOffices, &$clients, &$offices)
    {
        foreach ($requestOffices as $clientOffice) {
            if (strstr($clientOffice, '_')) {
                $parts = explode('_', $clientOffice);
                if (!in_array($parts[0], $clients)) {
                    $clients[] = $parts[0];
                }
                if (!in_array($parts[1], $offices)) {
                    $offices[] = $parts[1];
                }
            } else {
                if (!in_array($clientOffice, $clients)) {
                    $clients[] = $clientOffice;
                }
            }
        }
    }

    public function delete(User $user)
    {
        return view('users.delete', compact('user'));
    }

    public function destroy(Request $request)
    {
        try {
            /**
             * @var User $user
             */
            $user = User::findOrFail($request->input('user_id'));
            if ($user == \Auth::user()) {
                flash()->error('No se puede eliminar a si mismo');
                return redirect()->route('users');
            }

            $user->delete();
            flash()->success('El usuario ha sido eliminado');
        } catch (\Exception $e) {
            flash()->error('Se ha producido un error al eliminar el usuario: ' . $e->getMessage());
            return redirect()->back()->withInput();
        }

        return redirect()->route('users');
    }

    /**
     * @param $userId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($userId)
    {
        /**
         * @var User $client
         */
        $user = User::withTrashed()->findOrFail($userId);

        if (!$user->trashed()) {
            flash()->info('Este usuario ya se encuentra activo');
            return redirect()->route('users');
        }

        $user->restore();
        flash()->success('El usuario ha sido restaurado');
        return redirect()->route('users');
    }
}

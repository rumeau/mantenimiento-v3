<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->authorize('update-profile');

        return view('profile.index', ['user' => user()]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request)
    {
        $this->authorize('update-profile');

        $rules = [
            'name' => 'required',
            'mobile' => $request->input('mobile', '') != '' ? 'sometimes|min:8|max:8' : '',
        ];

        if ($request->input('password', '') != '') {
            $rules['password'] = UserRequest::passwordRules();

            if (!Hash::check($request->input('password_old', ''), user()->password)) {
                flash()->error('La clave actual no coincide con nuestros registros');
                return redirect()->back()->withInput();
            }
        }

        $this->validate($request, $rules);

        $user = user();
        $user->name = $request->input('name');
        $user->mobile = $request->input('mobile');
        if (($newPassword = $request->input('password', '')) != '') {
            $user->password = bcrypt($newPassword);
        }

        $user->save();
        flash()->success('Sus datos han sido guardados');
        return redirect()->refresh();
    }
}

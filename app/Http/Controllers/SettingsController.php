<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->authorize('update-settings');

        return view('settings.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request)
    {
        $data = $request->only([
            'priority_tta',
            'session_ttl',
            'password_strength',
            'password_length',
            'job_types',
        ]);

        foreach ($data as $key => $value) {
            configger()->set($key, $value);
        }

        flash()->success('Las opciones de configuración han sido guardadas');
        return redirect()->route('settings');
    }
}

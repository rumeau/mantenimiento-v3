<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClientRequest;
use App\Models\Client;
use App\Models\User;
use App\Scopes\OrderByNameScope;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class ClientController extends Controller
{
    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        /**
         * @var Builder $query
         */
        $query = Client::withoutGlobalScope(OrderByNameScope::class)->withTrashed();
        $filter = $request->query('filter', []);
        if (isset($filter['sortField'])) {
            $query->orderBy($filter['sortField'], array_get($filter, 'sortOrder', 'asc'));
        }

        $clients = $query->paginate($request->query('limit', 2));

        /**
         * @var LengthAwarePaginator $clients
         */
        if ($request->isXmlHttpRequest()) {
            return [
                'data' => $clients->items(),
                'itemsCount' => $clients->total(),
            ];
        }

        return view('clients.index', [
            'clients' => $clients,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $users = $this->getValidUsersToAssign(null);

        return view('clients.create', compact('users'));
    }

    /**
     * @param ClientRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ClientRequest $request)
    {
        try {
            $client = Client::create($request->only('name', 'phone', 'address'));
            $client->users()->sync($request->input('users', []));
            $client->save();
            flash()->success('La empresa ha sido guardada');
        } catch (\Exception $e) {
            flash()->error('Se ha producido un error al guardar la empresa: ' . $e->getMessage());
            return redirect()->back()->withInput();
        }

        return redirect()->route('clients');
    }

    /**
     * @param Client $client
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Client $client)
    {
        $users = $this->getValidUsersToAssign($client);
        return view('clients.edit', compact('client', 'users'));
    }

    /**
     * @param ClientRequest $request
     * @param Client $client
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ClientRequest $request, Client $client)
    {
        try {
            $client->update($request->only('name', 'phone', 'address'));
            $client->users()->sync($request->input('users', []));
            $client->save();
            flash()->success('La empresa ha sido guardada');
        } catch (\Exception $e) {
            flash()->error('Se ha producido un error al guardar la empresa: ' . $e->getMessage());
            return redirect()->back()->withInput();
        }

        return redirect()->route('clients');
    }

    public function delete(Client $client)
    {
        return view('clients.delete', compact('client'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        try {
            /**
             * @var Client $client
             */
            $client = Client::findOrFail($request->input('client_id'));
            $client->offices()->delete();
            $client->delete();
            flash()->success('La empresa ha sido eliminada');
        } catch (\Exception $e) {
            flash()->error('Se ha producido un error al eliminar la empresa: ' . $e->getMessage());
            return redirect()->back()->withInput();
        }

        return redirect()->route('clients');
    }

    /**
     * @param $clientId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($clientId)
    {
        /**
         * @var Client $client
         */
        $client = Client::withTrashed()->findOrFail($clientId);

        if (!$client->trashed()) {
            flash()->info('Esta empresa ya se encuentra activa');
            return redirect()->route('clients');
        }

        $client->restore();
        $client->offices()->restore();
        flash()->success('La empresa ha sido restaurada');
        return redirect()->route('clients');
    }

    /**
     * @param bool $client
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    protected function getValidUsersToAssign($client = null)
    {
        $users = User::orderBy('name', 'asc')->whereHas('roles', function ($q) {
            $q->where('roles.slug', '=', 'client');
        });
        
        $users->doesntHave('clients', 'and', function ($q) use ($client) {
            if ($client instanceof Client) {
                $q->where('clients.id', '!=', $client->id);
            }
        });

        return $users->get();
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\JobRequest;
use App\Models\Client;
use App\Models\Image;
use App\Models\Job;
use App\Models\Office;
use App\Models\Product;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image as ImageFacade;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\File;

class JobController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('client', ['except' => ['client', 'postClient']]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function client()
    {
        return view('jobs.client');
    }

    /**
     * @param Request $request
     * @param Guard $auth
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postClient(Request $request, Guard $auth)
    {
        /**
         * @var User $user
         */
        $user = $auth->user();
        if (in_array($user->group(), ['admins', 'company'])) {
            $allowedClients = Client::all();
        } else {
            $allowedClients = $user->clients;
        }

        $this->validate($request, [
            'client' => 'required|in:' . $allowedClients->implode('id', ','),
        ]);

        $client = $allowedClients->where('id', $request->input('client'))->first();
        $user->setClientAttribute($client);

        return redirect()->route('jobs');
    }

    /**
     * @param Request $request
     * @param Guard $auth
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|array
     */
    public function index(Request $request, Guard $auth)
    {
        /**
         * @var User $user
         */
        $user = $auth->user();
        $query = Job::query();

        $filters = $request->input('filter', []);

        $query->with('office', 'author');
        $query->where('client_id', $user->client->id);
        if ($user->group() == 'default') {
            $offices = $user->offices;
            $query->whereIn('jobs.office_id', $offices->pluck('id'));
        }

        if ($title = array_get($filters, 'title', false)) {
            if (!empty($title)) {
                $query->where('jobs.title', 'LIKE', '%' . $title . '%');
            }
        }

        if ($author = array_get($filters, 'author.name', false)) {
            if (!empty($author)) {
                $query->join('users', function ($join) use ($author) {
                    $join->on('users.id', '=', 'jobs.author_id')
                        ->where('users.name', 'LIKE', '%' . $author . '%');
                });
            }
        }

        if ($officeId = array_get($filters, 'office_id', false)) {
            $query->where('jobs.office_id', $officeId);
        }

        if ($sortField = array_get($filters, 'sortField', false)) {
            $availabelSortFieldsMapping = [
                'id' => 'jobs.id',
                'priority' => 'jobs.priority',
            ];

            if (array_key_exists($sortField, $availabelSortFieldsMapping)) {
                $query->orderBy($availabelSortFieldsMapping[$sortField], array_get($filters, 'sortOrder', 'ASC'));
            }
        } else {
            $query->orderBy('jobs.id', 'DESC');
        }

        if ($status = array_get($filters, 'status', false)) {
            $query->where('jobs.status', (int) $status);
        }

        if ($priority = array_get($filters, 'priority', false)) {
            $query->where('jobs.priority', $priority);
        }

        if ($user->isRole('client')) {
            $query->where('jobs.status', '!=', Job::STATUS_PENDING);
        }

        $jobs = $query->paginate();

        if ($request->isXmlHttpRequest()) {
            return [
                'data' => $jobs->items(),
                'itemsCount' => $jobs->total(),
            ];
        }


        return view('jobs.index');
    }

    /**
     * @param Guard $auth
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Guard $auth)
    {
        /**
         * @var User $user
         */
        $user = $auth->user();
        if ($user->group() == 'default') {
            $offices = $user->offices()->where('client_id', $user->client->id)->get();
        } else {
            $offices = Office::where('client_id', $user->client->id)->get();
        }

        return view('jobs.create', compact('offices'));
    }

    public function store(JobRequest $request, Guard $auth)
    {
        /**
         * @var User $user
         */
        $user = $auth->user();
        $data = $request->only(['status', 'job_type', 'title', 'description', 'images']);
        $data['client_id']   = $user->client->id;
        $data['office_id']   = $request->input('office');
        $data['priority']    = Job::PRIORITY_NORMAL;
        $data['starting_at'] = Carbon::createFromFormat('d/m/Y', $request->input('starting_at'));
        $data['ending_at']   = Carbon::createFromFormat('d/m/Y', $request->input('ending_at'));
        $data['author_id']   = $user->id;
        $data['viewed']      = false;

        /**
         * @var Job $job
         */
        $job = Job::create($data);

        if ($request->file('images', false)) {
            $this->uploadJobFiles($job, $request->file('images'));
        }

        $this->updateCostDetails($job, $request->input('costs', []), $request->file('costs'));

        if ((int) $data['status'] === Job::STATUS_PENDING) {
            //$job->sendCreatedToClientsNotification();
            flash()->success('La OT ha sido ingresada y se encuentra pendiente de Cotización!');
        } elseif ((int) $data['status'] === Job::STATUS_INPROGRESS) {
            $job->inprogress_at = Carbon::now();
            $job->save();
            $job->sendCreatedToClientsNotification();
            flash()->success('La OT ha sido ingresada!');
        }

        return redirect()->route('jobs');
    }

    public function edit(Request $request, Job $job, Guard $auth)
    {
        /**
         * @var User $user
         */
        $user = $auth->user();
        if ($user->group() == 'default') {
            $offices = $user->offices()->where('client_id', $user->client->id)->get();
        } else {
            $offices = Office::where('client_id', $user->client->id)->get();
        }

        $productsGroup = $job->products;
        $products = [];
        foreach ($productsGroup as $product) {
            if ($product->role === 1) {
                $role = 'materials';
            } elseif ($product->role === 2) {
                $role = 'rrhh';
            } elseif ($product->role === 3) {
                $role = 'quote';
            } elseif ($product->role === 4) {
                $role = 'other';
            } elseif ($product->role === 5) {
                $role = 'extra';
            }
            $products[$role][$product->id] = $product;
        }
        $images   = $job->images()->select('fullurl as imageUrl', DB::raw("'' as caption"))->get();
        return view('jobs.edit', compact('job', 'products', 'images', 'offices'));
    }

    /**
     * @param JobRequest $request
     * @param Job $job
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(JobRequest $request, Job $job)
    {
        $data = $request->only(['job_type', 'title', 'description', 'status']);
        $data['starting_at'] = Carbon::createFromFormat('d/m/Y', $request->input('starting_at'));
        $data['ending_at']   = Carbon::createFromFormat('d/m/Y', $request->input('ending_at'));

        $job->update($data);

        $this->updateCostDetails($job, $request->input('costs', []), $request->file('costs'));

        $saveMode = $request->input('save_mode', 'save');
        if ($saveMode === 'store') {
            $job->status = Job::STATUS_INPROGRESS;
            $job->inprogress_at = Carbon::now();
            $job->save();
            $job->sendCreatedToClientsNotification();
            flash()->success('La OT ha sido ingresada!');
        } else {
            flash()->success('La OT ha sido ingresada y se encuentra pendiente de Cotización!');
        }


        return redirect()->route('jobs');
    }

    /**
     * @param Request $request
     * @param Job $job
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePriority(Request $request, Job $job)
    {
        try {
            $this->authorize('updateJobPriority', $job);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'No puede modificar la prioridad de esta OT'
            ]);
        }

        if ((int) $job->status !== Job::STATUS_INPROGRESS) {
            return response()->json([
                'success' => false,
                'message' => 'No puede modificar una OT que no se encuentr EN PROGRESO',
            ]);
        }

        $job->priority = $request->input('priority', Job::PRIORITY_NORMAL);
        $job->save();

        if ((int) $job->priority === Job::PRIORITY_URGENT) {
            $job->sendPriorityUpdateToCompanyNotification();
        }

        $html = '';
        if ((int) $job->priority === \App\Models\Job::PRIORITY_NORMAL) {
            $html = '<span class="label label-default">Normal</span>';
        } elseif ((int) $job->priority === \App\Models\Job::PRIORITY_URGENT) {
            $html = '<span class="label label-danger">Urgente</span>';
        }

        return response()->json([
            'success' => true,
            'html' => $html,
        ]);
    }

    /**
     * @param Job $job
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Job $job, Guard $auth)
    {
        /**
         * @var User $user
         */
        $user = $auth->user();
        if ($user->isRole('client')) {
            if ((int) $job->viewed === 0) {
                $job->viewed = 1;
                $job->save();
            }

            if ((int) $job->status === Job::STATUS_COMPLETED && (int) $job->viewed !== 2) {
                $job->viewed = 2;
                $job->save();
            }
        }

        $costs  = $job->products;
        $images   = $job->images()->select('fullurl as imageUrl', DB::raw("'' as caption"))->get();
        return view('jobs.view', compact('job', 'costs', 'images'));
    }

    /**
     * @param Job $job
     * @param array $files
     */
    protected function uploadJobFiles(Job $job, $files = [])
    {
        foreach ($files as $file) {
            $filePath = $this->uploadFile($file);

            $image = Image::create([
                'job_id' => $job->id,
                'name' => $filePath,
                'path' => $filePath,
                'fullurl' => Storage::disk(config('filesystems.public_disk'))->url($filePath),
                'orig_name' => $file->getClientOriginalName(),
            ]);

            $job->images()->save($image);
        }
    }

    /**
     * @param Product $quote
     * @param UploadedFile $file
     */
    protected function uploadQuoteFile(Product $quote, UploadedFile $file)
    {
        /**
         * @var UploadedFile $file
         */
        // resizing an uploaded file
        $filePath = $file->store('uploaded', config('filesystems.public_disk'));

        $quote->update([
            'file_name' => $filePath,
            'file_path' => $filePath,
            'file_fullurl' => Storage::disk(config('filesystems.public_disk'))->url($filePath),
            'file_orig_name' => $file->getClientOriginalName(),
        ]);
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    protected function uploadFile(UploadedFile $file)
    {
        /**
         * @var UploadedFile $file
         */
        // resizing an uploaded file
        //Archivo temporal guardado en local
        $tmp = $file->store('uploads', 'local');
        $filename = str_replace('uploads/', '', $tmp);
        $filePath = Storage::disk('local')->getAdapter()->applyPathPrefix($tmp);
        // Lo redimensionamos y guardamos en el mismo lugar
        ImageFacade::make($filePath)
            ->resize(800, 600, function (Constraint $constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })
            ->save($filePath, 70);

        /**
         * @var UploadedFile $fileObject
         */
        // Guardar en su ubicacion final
        $newFilePath = $this->storeFile($filePath);
        Storage::disk('local')->delete($tmp);

        return $newFilePath;
    }

    /**
     * @param $filePath
     * @return string
     */
    protected function storeFile($filePath)
    {
        return Storage::disk(config('filesystems.public_disk', 'local'))
            ->putFile('uploaded', new File($filePath));
    }

    protected function updateCostDetails(Job $job, $costs, $files = [])
    {
        $newProducts = [];
        foreach ($costs as $role => $cost) {
            if ($role === 'materials' || $role === 'rrhh') {
                foreach ($cost as $product) {
                    $storedProduct = Product::find($product['id']);
                    $newProducts[$product['id']] = [
                        'total' => $product['amount'] * $storedProduct->cost,
                        'cost' => $storedProduct->cost,
                        'amount' => $product['amount']
                    ];
                }
            } else {
                // empty ID products needs to be inserted
                foreach ($cost as $key => $product) {
                    if ($role === 'quote') {
                        if (empty($product['id'])) {
                            $newProduct = $this->createNewProduct($product, array_get($files, 'quote.' . $key . '.file', null));
                            $product['amount'] = 1;
                        } else {
                            $newProduct = Product::find($product['id']);
                            $newProduct->cost = $product['cost'];
                            $newProduct->description = $product['description'];
                            $newProduct->save();
                        }

                        $newProducts[$newProduct->id] = [
                            'total' => 1 * $product['cost'],
                            'cost' => $product['cost'],
                            'amount' => 1
                        ];
                    } else {
                        if (empty($product['id'])) {
                            $newProduct = $this->createNewProduct($product);
                        } else {
                            $newProduct = Product::find($product['id']);
                            $newProduct->product = $product['product'];
                            $newProduct->cost = $product['cost'];
                            $newProduct->save();
                        }

                        $total = $role === 'extra' ? $newProduct->cost : $product['amount'] * $newProduct->cost;
                        $newProducts[$newProduct->id] = [
                            'cost' => $newProduct->cost,
                            'total' => $total,
                            'amount' => array_get($product, 'amount', 1)
                        ];
                    }
                }
            }
        }

        $job->products()->sync($newProducts);
    }

    /**
     * @param array $product
     * @param null $file
     * @return Product|\Illuminate\Database\Eloquent\Model $product
     */
    protected function createNewProduct($product = [], $file = null)
    {
        if ($product['role'] == Product::PRODUCT_TYPE_QUOTE) {
            /**
             * @var Product $product
             */
            $product = Product::create([
                'type' => 'quote',
                'description' => $product['description'],
                'cost' => $product['cost'],
                'role' => Product::PRODUCT_TYPE_QUOTE,
            ]);

            if (!is_null($file)) {
                $this->uploadQuoteFile($product, $file);
            }

            return $product;
        } elseif ($product['role'] == Product::PRODUCT_TYPE_OTHER) {
            return Product::create([
                'type' => $product['type'],
                'product' => $product['product'],
                'cost' => $product['cost'],
                'role' => Product::PRODUCT_TYPE_OTHER,
            ]);
        } elseif ($product['role'] == Product::PRODUCT_TYPE_EXTRA) {
            return Product::create([
                'type' => $product['type'],
                'product' => $product['product'],
                'cost' => $product['cost'],
                'role' => Product::PRODUCT_TYPE_EXTRA,
            ]);
        }

        return null;
    }

    /**
     * @return string
     */
    public function costProducts()
    {
        $materials = Product::groupBy('type')->where('role', 1)->get(['type']);
        $products  = Product::where('role', 1)
            ->get()
            ->groupBy('type')
            ->map(function ($c) {
                return $c->keyBy('id');
            });
        $rrhh      = Product::where('role', 2)->get()->keyBy('id');

        return compact('materials', 'products', 'rrhh');
    }

    public function complete(Job $job)
    {
        try {
            $this->authorize('complete', $job);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'No puede finalizar esta OT'
            ]);
        }

        $job->status = Job::STATUS_COMPLETED;
        $job->ended_at = Carbon::now();
        $job->save();

        $job->sendCompletedToClientNotification();

        return response()->json([
            'success' => true,
        ]);
    }

    public function rate(Request $request, Job $job)
    {
        $do = $request->input('do', null);
        $action = 'modificar';
        if ($do === null) {
            return response()->json([
                'success' => false,
                'message' => 'No puede modificar esta OT'
            ]);
        } else {
            if ($do === 'approve') {
                $action = 'aprobar';
                $status = Job::STATUS_RECEIVED;
            } elseif ($do === 'reject') {
                $action = 'rechazar';
                $status = Job::STATUS_REJECTED;
            }
        }

        try {
            $this->authorize('rate', $job);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'No puede ' . $action . ' esta OT'
            ]);
        }

        if (isset($status)) {
            $job->status = $status;
            $job->save();

            $job->sendRatedToCompanyNotification();

            return response()->json([
                'success' => true,
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'No puede modificar esta OT'
            ]);
        }
    }
}

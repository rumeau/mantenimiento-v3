<?php

namespace App\Http\Controllers;

use App\Http\Requests\OfficeRequest;
use App\Models\Client;
use App\Models\Office;
use App\Models\User;
use App\Scopes\OrderByNameScope;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

class OfficeController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|array
     */
    public function index(Request $request)
    {
        $query = Office::withoutGlobalScope(OrderByNameScope::class)->with('client');
        $filter = $request->query('filter', []);
        if (isset($filter['client_id']) && $filter['client_id'] > 0) {
            $query->where('client_id', $filter['client_id']);
        }
        
        if (isset($filter['sortField'])) {
            $query->orderBy($filter['sortField'], array_get($filter, 'sortOrder', 'asc'));
        }

        $offices = $query->paginate($request->query('limit', 2));

        /**
         * @var LengthAwarePaginator $clients
         */
        if ($request->isXmlHttpRequest()) {
            return [
                'data' => $offices->items(),
                'itemsCount' => $offices->total(),
            ];
        }

        return view('offices.index', [
            'offices' => $offices,
            'clients' => Client::orderBy('name', 'asc')->get()
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $clients = Client::orderBy('name', 'asc')->get();
        return view('offices.create', compact('clients'));
    }

    /**
     * @param OfficeRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(OfficeRequest $request)
    {
        try {
            /**
             * @var Office $office
             */
            $office = Office::create($request->only('name', 'phone', 'address', 'client_id'));
            $office->users()->sync($request->input('users'));
            $office->save();
            flash()->success('La sede ha sido guardada');
        } catch (\Exception $e) {
            flash()->error('Se ha producido un error al guardar la sede: ' . $e->getMessage());
            return redirect()->back()->withInput();
        }

        return redirect()->route('offices');
    }

    /**
     * @param Office $office
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Office $office)
    {
        $clients = Client::orderBy('name', 'asc')->get();

        $office->with('users')->get();
        return view('offices.edit', compact('office', 'clients'));
    }

    /**
     * @param OfficeRequest $request
     * @param Office $office
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(OfficeRequest $request, Office $office)
    {
        // checkeo
        $officeExists = Office::where('client_id', $request->input('client_id'))
            ->where('name', $request->input('name'))
            ->where('id', '!=', $office->id)
            ->exists();
        if ($officeExists) {
            flash()->error('El cliente seleccionado ya tiene una sede con ese nombre');
            return redirect()->back()->withInput();
        }

        try {
            $office->update($request->only('name', 'phone', 'address', 'client_id'));
            $office->users()->sync($request->input('users'));
            $office->save();
            flash()->success('La sede ha sido guardada');
        } catch (\Exception $e) {
            flash()->error('Se ha producido un error al guardar la sede: ' . $e->getMessage());
            return redirect()->back()->withInput();
        }

        return redirect()->route('offices');
    }

    /**
     * @param Request $request
     * @param Client $client
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function users(Request $request, Client $client)
    {
        $this->authorize('list-client-users');

        return $this->getValidUsersToAssign($client);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */

    protected function getValidUsersToAssign(Client $client)
    {
        $users = $client->users()->orderBy('name', 'asc')->whereHas('roles', function ($q) {
            $q->where('roles.slug', '=', 'client');
        })->get();

        return $users;
    }
}
